<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_steps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('campaign_id');
            $table->integer('newsletter_id');
            $table->integer('delay')->default(0);
            $table->integer('order');
            $table->boolean('queued')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign_steps');
    }
}
