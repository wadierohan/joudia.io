<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SomeFieldsShouldBeNullableInFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('forms', function (Blueprint $table) {
            $table->string('redirect')->nullable()->change();
            $table->text('success_msg')->nullable()->change();
            $table->text('welcome_subject')->nullable()->change();
            $table->text('welcome_body')->nullable()->change();
            $table->integer('delay')->default(0)->change();
            $table->text('html')->nullable()->change();
            $table->text('css')->nullable()->change();
            $table->text('js')->nullable()->change();
            $table->integer('visitors')->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('forms', function (Blueprint $table) {
            //
        });
    }
}
