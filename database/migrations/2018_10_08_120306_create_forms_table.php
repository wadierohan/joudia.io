<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('formtype_id');
            $table->string('redirect');
            $table->text('success_msg');
            $table->text('welcome_subject');
            $table->text('welcome_body');
            $table->integer('sender_id');
            $table->integer('delay');
            $table->integer('user_id');
            $table->string('uniq_id', 191)->unique();
            $table->text('html');
            $table->text('css');
            $table->text('js');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forms');
    }
}
