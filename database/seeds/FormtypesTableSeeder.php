<?php

use Illuminate\Database\Seeder;

class FormtypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' => 'form'],
            ['name' => 'popup'],
            ['name' => 'survey'],
        ];
        DB::table('Formtypes')->insert($data);
    }
}
