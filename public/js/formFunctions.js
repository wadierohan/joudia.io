/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/formFunctions.js":
/*!***************************************!*\
  !*** ./resources/js/formFunctions.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var validateEmail = function validateEmail(email) {
  var re = /^(([^<>()\[\]\.,;:\s@"]+(\.[^<>()\[\]\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};

var getElementsValue = function getElementsValue(elements, tag, uniqid) {
  var obj = {};

  for (var i = 0; i < elements.length; i++) {
    var item = elements.item(i);

    if (item.name.length > 0) {
      obj[item.name] = item.value;
    }
  }

  var array = [];
  array.push("tag=" + tag);
  array.push("uniqid=" + uniqid);

  for (var prop in obj) {
    array.push(prop + "=" + obj[prop]);
  }

  return array.join("&");
};

var showErrors = function showErrors(form, result) {
  var joudiaValidationError = form.querySelector('.joudiaValidationError');

  if (joudiaValidationError !== null) {
    joudiaValidationError.parentNode.removeChild(joudiaValidationError);
  }

  var container = document.createElement("ul");
  container.classList.add("joudiaValidationError");
  container.style.border = "1px solid #ebccd1";
  container.style.borderRadius = "4px";
  container.style.paddingTop = "5px";
  container.style.paddingBottom = "5px";
  container.style.color = "#a94442";
  container.style.backgroundColor = "#f2dede";
  container.style.setProperty("-webkit-transition", "1s ease-in-out");
  container.style.setProperty("-moz-transition", "1s ease-in-out");
  container.style.setProperty("-o-transition", "1s ease-in-out");
  container.style.transition = "1s ease-in-out";
  container.style.opacity = "0";

  for (var field in result.errors) {
    var fieldContainer = document.createElement("li");
    var fieldTitle = document.createTextNode(field);
    fieldContainer.appendChild(fieldTitle);

    if (result.errors.hasOwnProperty(field)) {
      (function () {
        var errors = result.errors[field];
        var errorsContainer = document.createElement("ul");
        errors.forEach(function (error) {
          var errorContainer = document.createElement("li");
          var errorTitle = document.createTextNode(error);
          errorContainer.appendChild(errorTitle);
          errorsContainer.appendChild(errorContainer);
        });
        fieldContainer.appendChild(errorsContainer);
      })();
    }

    container.appendChild(fieldContainer);
  }

  form.prepend(container);
  setTimeout(function () {
    container.style.opacity = "1";
  }, 300);
};

showModal = function showModal(form) {
  form.style.dislpay = "block";
  form.style.position = "fixed";
  form.style.top = "50%";
  form.style.left = "50%";
  form.style.setProperty("-ms-transform", "translate(-50%, -50%)");
  form.style.setProperty("transform", "translate(-50%, -50%)");
  form.style.border = "1px solid #ccc";
  form.style.borderRadius = "4px";
};

submitJoudiaForm = function submitJoudiaForm(form, e) {
  e.preventDefault();
  var tag = form.dataset.tag;
  var url = form.action;
  var uniqid = form.closest('.joudiaContainer').dataset.uniqid;
  var fields = getElementsValue(form.elements, tag, uniqid);
  var email = form.elements['email'].value;
  var submitButton = form.querySelector('button[type="submit"]');
  var savedSubmitButton = submitButton.innerHTML;

  if (validateEmail(email)) {
    submitButton.disabled = true;
    submitButton.innerHTML = "Veuillez patienter..";
    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
      if (this.readyState == 4) {
        if (this.status == 200) {
          var result = JSON.parse(this.responseText);

          if (result.redirect != "") {
            setTimeout(function () {
              document.location.href = result.redirect;
            }, 5000);
          } else if (result.success_msg != "") {
            form.innerHTML = result.success_msg;
          }

          if (result.isPopup) {
            setTimeout(function () {
              form.closest('.joudiaContainer').style.display = "none";
            }, 5000);
          }

          if (result.success_msg == "" && result.redirect == "") {
            form.innerHTML = "Vous vous êtes inscrit avec succès";
          }
        } else if (this.status == 422) {
          showErrors(form, JSON.parse(this.responseText));
          submitButton.disabled = false;
          submitButton.innerHTML = savedSubmitButton;
        } else {
          submitButton.innerHTML = "Oops! Une erreur est survenue lors de l\'inscription.";
        }
      }
    };

    xhttp.open("POST", url, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded"); //xhttp.setRequestHeader("Access-Control-Allow-Methods", "POST");
    //xhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    xhttp.send(fields);
  } else {
    alert("Veuillez saisir une adresse email valide.");
  }
};

/***/ }),

/***/ 2:
/*!*********************************************!*\
  !*** multi ./resources/js/formFunctions.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/wadie/html/joudia.test/resources/js/formFunctions.js */"./resources/js/formFunctions.js");


/***/ })

/******/ });