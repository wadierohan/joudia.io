/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/getform.js":
/*!*********************************!*\
  !*** ./resources/js/getform.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var getNodes = function getNodes(str) {
  return new DOMParser().parseFromString(str, 'text/html').body.childNodes;
};

var thisScript = document.currentScript;

var initJoudiaFunctions = function initJoudiaFunctions() {
  var functionScriptSrc = thisScript.src.replace('getform', 'formFunctions');

  if (document.querySelector("script[src=\"".concat(functionScriptSrc, "\"]")) === null) {
    var scriptTag = document.createElement("script");
    scriptTag.src = functionScriptSrc;

    scriptTag.onreadystatechange = function () {
      //This is for IE
      if (this.readyState == 'complete') {
        initJoudiaFunctions();
      }

      ;
    };

    scriptTag.onload = initJoudiaFunctions;
    document.getElementsByTagName("body")[0].appendChild(scriptTag);
  } else {
    var uniqid = thisScript.dataset.uniqid;
    var url = thisScript.src.replace('js/getform.js', 'api/forms/getForm');
    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        var result = JSON.parse(this.responseText);
        var tag = document.createElement("style");
        var t = document.createTextNode(result.css);
        tag.appendChild(t);
        document.getElementsByTagName("head")[0].appendChild(tag);
        console.log(result.isPopup);

        if (!result.isPopup) {
          var containerTag = document.createElement("section");
          containerTag.dataset.uniqid = uniqid;
          containerTag.classList.add("joudiaContainer");
        } else {
          var containerTag = document.createElement("div");
          containerTag.dataset.uniqid = uniqid;
          containerTag.classList.add("joudiaContainer");
          containerTag.classList.add("joudia-modal");
          var modalHeader = document.createElement("div");
          modalHeader.classList.add("joudia-modal-header");
          var closeBtn = document.createElement("span");
          closeBtn.classList.add("joudia-close-modal");
          closeBtn.innerHTML = '&times;';
          closeBtn.style["float"] = 'right';
          closeBtn.style.cursor = 'pointer';
          closeBtn.style.padding = '5px';

          closeBtn.onclick = function () {
            closeBtn.closest('.joudiaContainer').style.display = 'none';
          };

          modalHeader.style.padding = '10px';
          modalHeader.style.display = 'flow-root';
          modalHeader.appendChild(closeBtn);
          containerTag.appendChild(modalHeader);
        }

        var containerHtmlNodes = Array.prototype.slice.call(getNodes(result.html));

        for (var i = 0; i < containerHtmlNodes.length; i++) {
          containerTag.appendChild(containerHtmlNodes[i]);
        }

        thisScript.replaceWith(containerTag);

        if (result.isPopup) {
          showModal(containerTag);
        }
      }
    };

    xhttp.open("POST", url, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded"); //xhttp.setRequestHeader("Access-Control-Allow-Methods", "POST");
    //xhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    xhttp.send("uniqid=" + uniqid);
  }
};

initJoudiaFunctions();

/***/ }),

/***/ 1:
/*!***************************************!*\
  !*** multi ./resources/js/getform.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/wadie/html/joudia.test/resources/js/getform.js */"./resources/js/getform.js");


/***/ })

/******/ });