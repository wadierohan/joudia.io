
	tinymce.PluginManager.add('tag', function (editor, url) {
		editor.on('init', function() {
			editor.dom.loadCSS(url+'/css/tag.css')
		})
		var tags = editor.getParam("tags");
		var menuItems = [];
		tinymce.each(tags, function (tag, name) {
			menuItems.push({
				text: name,
				onclick: function () {
					editor.insertContent('<span class="badge badge-primary" contenteditable="false" data-field="'+tag+'">'+name+'</span>');
				}
			});
		});

		editor.addButton('tag', {
			type: 'menubutton',
			text: 'Tags',
			icon: 'code',
			menu: menuItems
		});

		editor.addMenuItem('tagDropDownMenu', {
			icon: 'code',
			text: 'Tags',
			menu: menuItems,
			context: 'insert',
			prependToContext: true
		});
	});
