<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::pattern('subdomain', '^((?!www|mailing).)*$');

Route::domain('{subdomain}.'.(env('APP_DOMAIN') !== null ? env('APP_DOMAIN') : 'joudia.io'))->group(function () {
    Route::get('login', 'AuthSubscriber\LoginController@showLoginForm')->name('subscriber.login');
    Route::post('login', 'AuthSubscriber\LoginController@login');
    Route::post('logout', 'AuthSubscriber\LoginController@logout')->name('subscriber.logout');
    Route::get('password/reset', 'AuthSubscriber\ForgotPasswordController@showLinkRequestForm')->name('subscriber.password.request');
    Route::post('password/email', 'AuthSubscriber\ForgotPasswordController@sendResetLinkEmail')->name('subscriber.password.email');
    Route::get('password/reset/{token}', 'AuthSubscriber\ResetPasswordController@showResetForm')->name('subscriber.password.reset');
    Route::post('password/reset', 'AuthSubscriber\ResetPasswordController@reset')->name('subscriber.password.update');
});

Auth::routes(['verify' => true]);

Route::group(['middleware' => 'auth:subscriber'], function () {
    Route::domain('{subdomain}.'.(env('APP_DOMAIN') !== null ? env('APP_DOMAIN') : 'joudia.io'))->group(function () {
        Route::get('/', 'DashboardController@formateurIndex')->name('formateur.index');
        Route::prefix('formation/{url}')->group(function () {
            Route::get('/', 'FormationController@formationSubscriber')->name('formateur.formation');
            Route::prefix('module/{module}')->group(function () {
                Route::get('/', 'ModuleController@moduleSubscriber')->name('formateur.module');
                Route::get('chapter/{chapter}', 'ChapterController@chapterSubscriber')->name('formateur.chapter');
            });
        });
    });
});

Route::group(['middleware' => ['auth:web', 'verified']], function () {
    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::resource('mailservers', 'MailserverController');
    Route::resource('senders', 'SenderController');
    Route::resource('newsletters', 'NewsletterController');
    Route::resource('tags', 'TagController');
    Route::resource('subscribers', 'SubscriberController');
    Route::resource('forms', 'FormController');
    Route::resource('schedules', 'ScheduleController');
    Route::resource('campaigns', 'CampaignController');
    Route::resource('formations', 'FormationController');
    Route::resource('fields', 'FieldController');

    Route::prefix('formation/{formation}')->group(function () {
        Route::post('students', 'FormationController@storeStudents')->name('students.store');
        Route::get('students', 'FormationController@indexStudents')->name('students.index');
        Route::get('students/edit', 'FormationController@editStudents')->name('students.edit');
        Route::resource('modules', 'ModuleController');
        Route::prefix('module/{module}')->group(function () {
            Route::resource('chapters', 'ChapterController');
            Route::get('chapters/{chapter}/wysiwyg', 'ChapterController@wysiwyg')->name('chapters.wysiwyg');
            Route::patch('chapters/{chapter}/wysiwyg', 'ChapterController@wysiwygUpdate')->name('chapters.wysiwyg.update');
        });
    });

    Route::get('forms/{form}/wysiwyg', 'FormController@wysiwyg')->name('forms.wysiwyg');
    Route::patch('forms/{form}/wysiwyg', 'FormController@wysiwygUpdate')->name('forms.wysiwyg.update');
    Route::get('queues', 'QueueController@index')->name('queues.index');
    Route::get('queues/schedule/{schedule}', 'QueueController@showSchedule')->name('queues.schedule');
    Route::get('queues/subscriber/{subscriber}', 'QueueController@showSubscriber')->name('queues.subscriber');
    Route::get('queues/newsletter/{newsletter}', 'QueueController@showNewsletter')->name('queues.newsletter');
    Route::delete('queues/{queue}', 'QueueController@destroy')->name('queues.destroy');
    Route::get('campaigns/{campaign}/automate', 'CampaignController@automate')->name('campaigns.automate');
    Route::patch('campaigns/{campaign}/automate', 'CampaignController@automateUpdate')->name('campaigns.automate.update');
});
