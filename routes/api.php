<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('get_file_input', 'Api\NewsletterController@get_file_input')->name('get_file_input');
    Route::post('delete_newsletter_file', 'Api\NewsletterController@delete_newsletter_file')->name('delete_newsletter_file');

    Route::post('uploaduserimage', 'Api\GjsController@uploaduserimage')->name('uploaduserimage');
    Route::post('storegjs', 'Api\GjsController@storegjs')->name('storegjs');
    Route::get('loadgjs/{userid}', 'Api\GjsController@loadgjs')->name('loadgjs');

    Route::post('subscribers/search', 'Api\SubscriberController@search');
    Route::post('tags', 'Api\TagController@index');
    Route::post('forms', 'Api\FormController@index');
    Route::post('formations', 'Api\FormationController@index');
});

Route::post('forms/getForm', 'Api\FormController@getForm')->name('forms.getForm');
Route::post('forms/submitForm', 'Api\FormController@submitForm')->name('forms.submitForm');
Route::get('queues/{queue}/opened/transparent.gif', 'Api\QueueController@opened')->name('queue.opened')->where(['queue' => '[0-9]+']);
Route::post('tags/create', 'Api\TagController@create')->name('tags.apistore');
