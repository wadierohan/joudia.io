<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mailserver extends Model
{
    protected $fillable = [
        'name', 'driver', 'host', 'port', 'encryption', 'username', 'password', 'user_id'
    ];

    protected $hidden = [
        'password'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function senders()
    {
        return $this->hasMany('App\Sender');
    }
}
