<?php

namespace App\Policies;

use App\User;
use App\Mailserver;
use Illuminate\Auth\Access\HandlesAuthorization;

class MailserverPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the mailserver.
     *
     * @param  \App\User  $user
     * @param  \App\Mailserver  $mailserver
     * @return mixed
     */
    public function view(User $user, Mailserver $mailserver)
    {
        return $user->id === $mailserver->user_id;
    }

    /**
     * Determine whether the user can create mailservers.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the mailserver.
     *
     * @param  \App\User  $user
     * @param  \App\Mailserver  $mailserver
     * @return mixed
     */
    public function update(User $user, Mailserver $mailserver)
    {
        return $user->id === $mailserver->user_id;
    }

    /**
     * Determine whether the user can delete the mailserver.
     *
     * @param  \App\User  $user
     * @param  \App\Mailserver  $mailserver
     * @return mixed
     */
    public function delete(User $user, Mailserver $mailserver)
    {
        return $user->id === $mailserver->user_id;
    }

    /**
     * Determine whether the user can restore the mailserver.
     *
     * @param  \App\User  $user
     * @param  \App\Mailserver  $mailserver
     * @return mixed
     */
    public function restore(User $user, Mailserver $mailserver)
    {
        return $user->id === $mailserver->user_id;
    }

    /**
     * Determine whether the user can permanently delete the mailserver.
     *
     * @param  \App\User  $user
     * @param  \App\Mailserver  $mailserver
     * @return mixed
     */
    public function forceDelete(User $user, Mailserver $mailserver)
    {
        return $user->id === $mailserver->user_id;
    }
}
