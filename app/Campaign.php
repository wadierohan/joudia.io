<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    protected $fillable = [
        'name', 'user_id', 'trigger', 'trigger_value', 'executed', 'tags_type'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function form()
    {
        return $this->belongsTo('App\Form', 'trigger_value');
    }

    public function campaignSteps()
    {
        return $this->hasMany('App\CampaignStep');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }

    public function scopeTriggeredByForm($query, $form_id)
    {
        return $query->where(['trigger' => 'new_form_sub', 'trigger_value' => $form_id]);
    }

    public function scopeReadyTriggeredByDate($query)
    {
        return $query->where(['trigger' => 'at_date', 'executed' => false])->whereDate('trigger_value', '<=', Carbon::now());
    }

    public function scopeOwned($query)
    {
        return $query->where('campaigns.user_id', Auth::id());
    }

    public function delete()
    {
        $this->campaignSteps()->delete();
        parent::delete();
    }
}
