<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Formation extends Model
{
    protected $fillable = [
        'name', 'user_id', 'desc', 'url', 'img', 'enabled'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function modules()
    {
        return $this->hasMany('App\Module');
    }

    public function subscribers()
    {
        return $this->belongsToMany('App\Subscriber')->withTimestamps();
    }

    public function scopeByUrl($query, $url)
    {
        return $query->where(['url' => $url, 'enabled' => 1]);
    }

    public function scopeOwned($query)
    {
        return $query->where('formations.user_id', Auth::id());
    }
}
