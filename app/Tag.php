<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Illuminate\Support\Carbon;

class Tag extends Model
{
    protected $fillable = [
        'name', 'user_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function forms()
    {
        return $this->belongsToMany('App\Form');
    }

    public function subscribers()
    {
        return $this->belongsToMany('App\Subscriber')->withTimestamps();
    }

    public function campaigns()
    {
        return $this->belongsToMany('App\Campaign');
    }

    public function scopeOwned($query)
    {
        return $query->where('tags.user_id', Auth::id());
    }

    public function subscribedToday()
    {
        return $this->belongsToMany('App\Subscriber')->withTimestamps()->wherePivot('created_at', '>=', Carbon::today());
    }

    public function subscribedYesterday()
    {
        return $this->belongsToMany('App\Subscriber')->withTimestamps()->wherePivot('created_at', '<', Carbon::today())->wherePivot('created_at', '>=', Carbon::yesterday());
    }
}
