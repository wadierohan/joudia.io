<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sender extends Model
{
    protected $fillable = [
        'name', 'from_name', 'from_email', 'reply_name', 'reply_email', 'mailserver_id', 'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function mailserver()
    {
        return $this->belongsTo('App\Mailserver');
    }

    public function queues()
    {
        return $this->hasMany('App\Queue');
    }
}
