<?php

namespace App\Observers;

use App\Newsletter;
use Illuminate\Support\Facades\Storage;

class NewsletterObserver
{
    /**
     * Handle the newsletter "created" event.
     *
     * @param  \App\Newsletter  $newsletter
     * @return void
     */
    public function created(Newsletter $newsletter)
    {
        //
    }

    /**
     * Handle the newsletter "updated" event.
     *
     * @param  \App\Newsletter  $newsletter
     * @return void
     */
    public function updated(Newsletter $newsletter)
    {
        //
    }

    /**
     * Handle the newsletter "deleted" event.
     *
     * @param  \App\Newsletter  $newsletter
     * @return void
     */
    public function deleted(Newsletter $newsletter)
    {
        Storage::disk('newsletters')->deleteDirectory($newsletter->id);
    }

    /**
     * Handle the newsletter "restored" event.
     *
     * @param  \App\Newsletter  $newsletter
     * @return void
     */
    public function restored(Newsletter $newsletter)
    {
        //
    }

    /**
     * Handle the newsletter "force deleted" event.
     *
     * @param  \App\Newsletter  $newsletter
     * @return void
     */
    public function forceDeleted(Newsletter $newsletter)
    {
        //
    }
}
