<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignStep extends Model
{
    protected $fillable = [
        'campaign_id', 'newsletter_id', 'delay', 'order', 'queued'
    ];

    public function campaign()
    {
        return $this->belongsTo('App\Campaign');
    }

    public function newsletter()
    {
        return $this->belongsTo('App\Newsletter');
    }

    public function campaignprocesses()
    {
        return $this->hasMany('App\CampaignProcess');
    }

    public function scopeFirstStep($query)
    {
        return $query->where('order', 0);
    }

    public function scopeNextCampaignStep($query, $campaign_id, $order)
    {
        return $query->where(['order' => $order, 'campaign_id' => $campaign_id]);
    }

    public function delete()
    {
        $this->campaignprocesses()->delete();
        parent::delete();
    }
}
