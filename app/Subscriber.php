<?php

namespace App;

use Auth;
use App\User;
use Illuminate\Support\Facades\Route;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\CanResetPassword;
use App\Notifications\ResetPasswordNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Subscriber extends Authenticatable implements CanResetPassword
{
    use Notifiable;

    protected $table = 'subscribers';

    protected $guard = 'subscriber';

    protected $fillable = ['email',  'password'];

    protected $hidden = ['password',  'remember_token'];

    public function getHasPasswordAttribute()
    {
        return !empty($this->password);
    }

    public function scopeHasntPassword($query)
    {
        return $query->where('password', null);
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag')->withTimestamps();
    }

    public function ownedTags()
    {
        return $this->belongsToMany('App\Tag')->where('user_id', auth()->id())->withTimestamps();
    }

    public function forms()
    {
        return $this->belongsToMany('App\Form')->withTimestamps();
    }

    public function ownedForms()
    {
        return $this->belongsToMany('App\Form')->where('user_id', auth()->id())->withTimestamps();
    }

    public function formations()
    {
        return $this->belongsToMany('App\Formation')->withTimestamps();
    }

    public function ownedFormations()
    {
        return $this->belongsToMany('App\Formation')->where('user_id', auth()->id())->withTimestamps();
    }

    public function queues()
    {
        return $this->hasMany('App\Queue');
    }

    public function campaignprocesses()
    {
        return $this->hasMany('App\CampaignProcess');
    }
    
    public function fields()
    {
        return $this->belongsToMany('App\Field', 'field_subscriber', 'subscriber_id', 'field_id', 'id', 'uniq_id')->as('subscription')->withPivot('value')->withTimestamps();
    }

    public function ownedFields()
    {
        return $this->belongsToMany('App\Field', 'field_subscriber', 'subscriber_id', 'field_id', 'id', 'uniq_id')->where('user_id', auth()->id())->as('subscription')->withPivot('value')->withTimestamps();
    }

    public function fieldValue($field_id)
    {
        $field = $this->fields()->find($field_id);
        return $field ? $field->subscription->value : null;
    }

    public function isOwned()
    {
        return !empty(array_intersect($this->tags->pluck('tags.id')->toArray(), auth()->user()->tags->pluck('tags.id')->toArray()));
    }

    public function scopeOwned($query)
    {
        return $query->whereHas('tags', function ($q) {
            $q->whereIn('tag_id', Auth::user()->tags()->pluck('tags.id')->toArray());
        });
    }

    public function scopeOwnedBy($query, $user_id)
    {
        $user = User::findOrFail($user_id);
        return $query->whereHas('tags', function ($q) use ($user) {
            $q->whereIn('tag_id', $user->tags()->pluck('tags.id')->toArray());
        });
    }

    public function scopeHasAllTags($query, $tags)
    {
        return $query->whereHas('tags', function ($q) use ($tags) {
            $q->whereIn('tag_id', $tags);
        }, '=', count($tags));
    }

    public function scopeHasOneOfTags($query, $tags)
    {
        return $query->whereHas('tags', function ($q) use ($tags) {
            $q->whereIn('tag_id', $tags);
        });
    }

    public function scopeHasTag($query, $tag)
    {
        return $query->whereHas('tags', function ($q) use ($tag) {
            $q->where('tag_id', $tag);
        });
    }

    public function scopeHasForm($query, $form)
    {
        return $query->whereHas('forms', function ($q) use ($form) {
            $q->where('form_id', $form);
        });
    }

    public function scopeHasFormation($query, $formation)
    {
        return $query->whereHas('formations', function ($q) use ($formation) {
            $q->where('formation_id', $formation);
        });
    }

    public function scopeHasntTag($query, $tag)
    {
        return $query->whereDoesntHave('tags', function ($q) use ($tag) {
            $q->where('tag_id', $tag);
        });
    }

    public function scopeHasntForm($query, $form)
    {
        return $query->whereDoesntHave('forms', function ($q) use ($form) {
            $q->where('form_id', $form);
        });
    }

    public function scopeHasntFormation($query, $formation)
    {
        return $query->whereDoesntHave('formations', function ($q) use ($formation) {
            $q->where('formation_id', $formation);
        });
    }

    public function sendPasswordResetNotification($token)
    {
        $subdomain = auth('web')->check() ? auth('web')->user()->domain : Route::input('subdomain');
        $this->notify(new ResetPasswordNotification($subdomain, $token, auth('web')->check()));
    }
}
