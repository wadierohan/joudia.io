<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\FormCreated' => [
            'App\Listeners\GenerateFormJs',
        ],
        'App\Events\FormSubmited' => [
            'App\Listeners\CreateOrUpdateSubscriber',
            'App\Listeners\GetInCampaign',
            'App\Listeners\SendWelcomeEmail',
        ],
        'App\Events\Cron' => [
            'App\Listeners\PrepareReadyCampaign',
            'App\Listeners\UpdateCampaignProcess',
            'App\Listeners\PrepareReadySchedule',
            'App\Listeners\SendReadyQueue',
        ],
        'App\Events\ChapterCreated' => [
            'App\Listeners\GenerateChapterHtml',
        ],
        'App\Events\SubscriberAddedToFormation' => [
            'App\Listeners\GeneratePassword',
            'App\Listeners\WelcomeToFormation',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
