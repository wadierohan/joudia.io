<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Events\Cron;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CheckQueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'queue:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check all queues, campaign, schedules, and send emails';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info('['.Carbon::now()->format('Y-m-d H:i:s').'] Queue check running..');
        event(new Cron());
        Log::info('['.Carbon::now()->format('Y-m-d H:i:s').'] Queue check done.');
    }
}
