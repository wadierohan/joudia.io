<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Auth;

class Queue extends Model
{
    protected $fillable = [
        'newsletter_id', 'subscriber_id', 'user_id', 'try', 'send_at', 'sent', 'opened', 'opened_at', 'schedule_id', 'campaign_process_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function newsletter()
    {
        return $this->belongsTo('App\Newsletter');
    }

    public function subscriber()
    {
        return $this->belongsTo('App\Subscriber');
    }

    public function schedule()
    {
        return $this->belongsTo('App\Schedule');
    }

    public function campaignprocess()
    {
        return $this->belongsTo('App\CampaignProcess');
    }

    public function scopeOwned($query)
    {
        return $query->where('queues.user_id', Auth::id());
    }

    public function scopeToSend($query)
    {
        return $query->where('sent', false)->whereDate('send_at', '<=', Carbon::now()->toDateTimeString());
    }
}
