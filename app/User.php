<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, Notifiable;

    protected $fillable = [
        'name', 'email', 'password', 'domain',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function hasRole($selected_role)
    {
        foreach ($this->roles as $role) {
            if ($role->name == $selected_role) {
                return true;
            }
        }
        return false;
    }

    public function scopeSubdomain($query, $domain)
    {
        return $query->where('domain', $domain);
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    public function tags()
    {
        return $this->hasMany('App\Tag');
    }

    public function forms()
    {
        return $this->hasMany('App\Form');
    }

    public function senders()
    {
        return $this->hasMany('App\Sender');
    }

    public function newsletters()
    {
        return $this->hasMany('App\Newsletter');
    }

    public function mailservers()
    {
        return $this->hasMany('App\Mailserver');
    }

    public function fields()
    {
        return $this->hasMany('App\Field');
    }

    public function queues()
    {
        return $this->hasMany('App\Queue');
    }

    public function schedules()
    {
        return $this->hasMany('App\Schedule');
    }

    public function campaigns()
    {
        return $this->hasMany('App\Campaign');
    }

    public function formations()
    {
        return $this->hasMany('App\Formation');
    }

    public function delete()
    {
        $this->tags()->delete();
        $this->forms()->delete();
        $this->senders()->delete();
        $this->newsletters()->delete();
        $this->mailservers()->delete();
        $this->fields()->delete();
        $this->queues()->delete();
        $this->schedules()->delete();
        $this->campaigns()->delete();
        $this->formations()->delete();
        parent::delete();
    }
}
