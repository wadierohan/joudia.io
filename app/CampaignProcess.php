<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class CampaignProcess extends Model
{
    protected $fillable = [
        'campaign_step_id', 'subscriber_id', 'queued_at', 'queued'
    ];

    public function campaignStep()
    {
        return $this->belongsTo('App\CampaignStep');
    }

    public function subscriber()
    {
        return $this->belongsTo('App\Subscriber');
    }

    public function queue()
    {
        return $this->hasOne('App\Queue');
    }

    public function scopeToQueued($query)
    {
        return $query->where('queued', false)->whereDate('queued_at', '<=', Carbon::now());
    }
}
