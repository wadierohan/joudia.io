<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    protected $fillable = [
        'name', 'subject', 'body', 'type', 'sender_id', 'user_id'
    ];

    public function sender()
    {
        return $this->belongsTo('App\Sender');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function queues()
    {
        return $this->hasMany('App\Queue');
    }

    public function schedules()
    {
        return $this->hasMany('App\Schedule');
    }

    public function campaignsteps()
    {
        return $this->hasMany('App\CampaignStep');
    }

    public function delete()
    {
        $this->queues()->delete();
        $this->schedules()->delete();
        parent::delete();
    }
}
