<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Schedule extends Model
{
    protected $fillable = [
        'name', 'newsletter_id', 'user_id', 'tags_type', 'queued_at', 'queued'
    ];

    public function delete()
    {
        $this->queues()->delete();
        parent::delete();
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function newsletter()
    {
        return $this->belongsTo('App\Newsletter');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }

    public function queues()
    {
        return $this->hasMany('App\Queue');
    }

    public function scopeToQueued($query)
    {
        return $query->where('queued', false)->whereDate('queued_at', '<=', Carbon::now()->toDateTimeString());
    }
}
