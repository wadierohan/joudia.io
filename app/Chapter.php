<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    protected $fillable = [
        'name', 'module_id', 'desc', 'enabled', 'html'
    ];

    public function module()
    {
        return $this->belongsTo('App\Module');
    }
}
