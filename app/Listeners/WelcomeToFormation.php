<?php

namespace App\Listeners;

use App\Events\SubscriberAddedToFormation;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\SubscribedToFormation;

class WelcomeToFormation
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SubscriberAddedToFormation  $event
     * @return void
     */
    public function handle(SubscriberAddedToFormation $event)
    {
        // Send email to formation
        $subscriber = $event->subscriber;
        $formation = $event->formation;
        $subscriber->notify(new SubscribedToFormation($formation));
    }
}
