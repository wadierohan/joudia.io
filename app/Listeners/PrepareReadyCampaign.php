<?php

namespace App\Listeners;

use App\Events\Cron;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Campaign;
use App\Subscriber;
use Carbon\Carbon;

class PrepareReadyCampaign
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Cron  $event
     * @return void
     */
    public function handle(Cron $event)
    {
        $campaigns = Campaign::ReadyTriggeredByDate()->get();
        foreach ($campaigns as $campaign) {
            $firstStep = $campaign->campaignSteps()->firstStep()->first();
            if ($firstStep) {
                if ($campaign->tags_type == 'alltags') {
                    $subscribers = Subscriber::ownedBy($campaign->user_id)->get();
                } elseif ($campaign->tags_type == 'all') {
                    $subscribers = Subscriber::hasAllTags($campaign->tags()->pluck('tags.id')->toArray())->get();
                } elseif ($campaign->tags_type == 'once') {
                    $subscribers = Subscriber::hasOneOfTags($campaign->tags()->pluck('tags.id')->toArray())->get();
                }
                $data = [];
                foreach ($subscribers as $subscriber) {
                    $data[] = [
                        'campaign_step_id'  => $firstStep->id,
                        'subscriber_id'     => $subscriber->id,
                        'queued_at'         => Carbon::now(),
                    ];
                }
                $firstStep->campaignprocesses()->createMany($data);
            }
            $campaign->executed = true;
            $campaign->save();
        }
    }
}
