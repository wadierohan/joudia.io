<?php

namespace App\Listeners;

use App\Events\Cron;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Queue;
use App\Schedule;
use App\Subscriber;

class PrepareReadySchedule
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Cron  $event
     * @return void
     */
    public function handle(Cron $event)
    {
        $schedules = Schedule::toQueued()->get();
        foreach ($schedules as $schedule) {
            if ($schedule->tags_type == 'alltags') {
                $subscribers = Subscriber::ownedBy($schedule->user_id)->get();
            } elseif ($schedule->tags_type == 'all') {
                $subscribers = Subscriber::hasAllTags($schedule->tags()->pluck('tags.id')->toArray())->get();
            } elseif ($schedule->tags_type == 'once') {
                $subscribers = Subscriber::hasOneOfTags($schedule->tags()->pluck('tags.id')->toArray())->get();
            }
            $data = [];
            foreach ($subscribers as $subscriber) {
                $data[] = [
                    'newsletter_id' => $schedule->newsletter_id,
                    'subscriber_id' => $subscriber->id,
                    'user_id'       => $schedule->user_id,
                    'send_at'       => $schedule->queued_at,
                ];
            }
            $schedule->queues()->createMany($data);
            $schedule->queued = true;
            $schedule->save();
        }
    }
}
