<?php

namespace App\Listeners;

use App\Events\Cron;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Queue;
use App\CampaignProcess;
use App\CampaignStep;
use App\Subscriber;
use Carbon\Carbon;

class UpdateCampaignProcess
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Cron  $event
     * @return void
     */
    public function handle(Cron $event)
    {
        $campaignProcesses = CampaignProcess::toQueued()->get();
        foreach ($campaignProcesses as $campaignProcess) {
            $data = [
                'newsletter_id' => $campaignProcess->campaignStep->newsletter_id,
                'subscriber_id' => $campaignProcess->subscriber_id,
                'user_id'       => $campaignProcess->campaignStep->campaign->user_id,
                'send_at'       => $campaignProcess->queued_at,
            ];
            $campaignProcess->queue()->create($data);
            $campaignProcess->queued = true;
            $campaignProcess->save();
            $nextCampaignStep = CampaignStep::nextCampaignStep($campaignProcess->campaignStep->campaign_id, ($campaignProcess->campaignStep->order + 1))->first();
            if ($nextCampaignStep) {
                $nextCampaignProcess = new CampaignProcess();
                $nextCampaignProcess->campaign_step_id = $nextCampaignStep->id;
                $nextCampaignProcess->subscriber_id = $campaignProcess->subscriber_id;
                $nextCampaignProcess->queued_at = Carbon::now()->addSeconds($nextCampaignStep->delay);
                $nextCampaignProcess->save();
            }
        }
    }
}
