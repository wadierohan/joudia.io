<?php

namespace App\Listeners;

use App\Events\FormSubmited;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Form;
use App\Subscriber;
use App\Queue;
use Carbon\Carbon;

/**
 * Handle
 *
 * @package FormSubmited $event Comment
 */
class SendWelcomeEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle
     *
     * @param FormSubmited $event
     *
     * @return void
     */
    public function handle(FormSubmited $event)
    {
        $form = Form::uniqid($event->request->uniqid)->firstOrFail();
        $subscriber = Subscriber::where('email', $event->request->email)->firstOrFail();

        $email = $subscriber->email;
        if ($form->newsletter_id) {
            $queue = new Queue();
            $queue->newsletter_id = $form->newsletter_id;
            $queue->subscriber_id = $subscriber->id;
            $queue->user_id = $form->user_id;
            $queue->send_at = Carbon::now()->addSeconds($form->delay);
            $queue->save();
        }
    }
}
