<?php

namespace App\Listeners;

use Mail;
use App\Queue;
use App\Events\Cron;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendReadyQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Cron  $event
     * @return void
     */
    public function handle(Cron $event)
    {
        // Get ready queue
        $ready_newsletters = Queue::toSend()->get();
        foreach ($ready_newsletters as $queue) {
            if ($queue->newsletter && $queue->newsletter->sender && $queue->newsletter->sender->mailserver && $queue->try <= 3) {
                // Get config
                $mailConfig = ['mail' => [
                    'driver' => $queue->newsletter->sender->mailserver->driver,
                    'host' => $queue->newsletter->sender->mailserver->host,
                    'port' => $queue->newsletter->sender->mailserver->port,
                    'from' => [
                        'address' => $queue->newsletter->sender->from_email,
                        'name' => $queue->newsletter->sender->from_name,
                    ],
                    'reply_to' => [
                        'address' => $queue->newsletter->sender->reply_email,
                        'name' => $queue->newsletter->sender->reply_name,
                    ],
                    'encryption' => $queue->newsletter->sender->mailserver->encryption,
                    'username' => $queue->newsletter->sender->mailserver->username,
                    'password' => $queue->newsletter->sender->mailserver->password,
                ]];

                // Set config
                config($mailConfig);
                
                $subject = $queue->newsletter->subject;
                $body = $queue->newsletter->body;
                $fields = ['email' => $queue->subscriber->email];
                foreach ($queue->subscriber->fields as $field) {
                    $fields[$field->uniq_id] = $field->subscription->value;
                }

                $body = preg_replace_callback(
                    '/\<span class\=\"badge badge\-primary\" contenteditable\=\"false\" data\-field\=\"(.*?)\"\>(.*?)\<\/span\>/',
                    function ($match) use ($fields) {
                        return str_replace($match[0], isset($fields[$match[1]]) ? $fields[$match[1]] : '____', $match[0]);
                    },
                    $body
                );

                $files = Storage::disk('newsletters')->files($queue->newsletter->id);

                //Send email
                Mail::send('email', ['subject' => $subject, 'body' => $body, 'queue_id' => $queue->id, 'subscriber_id' => $queue->subscriber_id ], function ($m) use ($queue, $subject, $files) {
                    $m->to($queue->subscriber->email)->subject($subject);
                    
                    if ($files) {
                        foreach ($files as $file) {
                            $m->attach(Storage::disk('newsletters')->path($file));
                        }
                    }
                });
                
                if (count(Mail::failures()) > 0) {
                    $queue->try++;
                } else {
                    $queue->sent = 1;
                }
                $queue->save();
            } else {
                if ($queue->try > 3) {
                    $queue->delete();
                } else {
                    $queue->try++;
                    $queue->save();
                }
            }
        }
    }
}
