<?php

namespace App\Listeners;

use App\Events\FormSubmited;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Form;
use App\Subscriber;

class CreateOrUpdateSubscriber
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  FormSubmited  $event
     * @return void
     */
    public function handle(FormSubmited $event)
    {
        $form = Form::uniqid($event->request->uniqid)->firstOrFail();
        $subscriber = Subscriber::firstOrCreate(['email' => $event->request->email]);
        //$subscriber->tags()->syncWithoutDetaching($form->tags()->pluck('tags.id')->toArray());
        $subscriber->tags()->syncWithoutDetaching($event->request->tag);
    }
}
