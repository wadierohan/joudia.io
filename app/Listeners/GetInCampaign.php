<?php

namespace App\Listeners;

use App\Events\FormSubmited;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Form;
use App\Subscriber;
use App\Campaign;
use App\CampaignProcess;
use Carbon\Carbon;

class GetInCampaign
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  FormSubmited  $event
     * @return void
     */
    public function handle(FormSubmited $event)
    {
        $form = Form::uniqid($event->request->uniqid)->firstOrFail();
        $subscriber = Subscriber::where('email', $event->request->email)->firstOrFail();
        $campaign = Campaign::triggeredByForm($form->id)->first();
        if ($campaign) {
            $firstStep = $campaign->campaignSteps()->firstStep()->first();
            if ($firstStep) {
                $campaignProcess = new CampaignProcess();
                $campaignProcess->campaign_step_id = $firstStep->id;
                $campaignProcess->subscriber_id = $subscriber->id;
                $campaignProcess->queued_at = Carbon::now()->addSeconds($firstStep->delay);
                $campaignProcess->save();
            }
        }
    }
}
