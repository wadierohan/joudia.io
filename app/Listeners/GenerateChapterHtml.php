<?php

namespace App\Listeners;

use App\Events\ChapterCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class GenerateChapterHtml
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ChapterCreated  $event
     * @return void
     */
    public function handle(ChapterCreated $event)
    {
        /*
        $event->chapter->html = '<form id="'.$event->chapter->uniq_id.'" class="joudia-form">
        <link href="'.asset('css/joudia-form.css').'" rel="stylesheet">
        <ul></ul>
        </form>';
        $event->chapter->save();
        */
    }
}
