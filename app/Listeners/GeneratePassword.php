<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Password;
use App\Events\SubscriberAddedToFormation;
use Illuminate\Contracts\Queue\ShouldQueue;

class GeneratePassword
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SubscriberAddedToFormation  $event
     * @return void
     */
    public function handle(SubscriberAddedToFormation $event)
    {
        $subscriber = $event->subscriber;
        if (is_null($subscriber->password)) {
            $credentials = ['email' => $subscriber->email];
            Password::broker('subscribers')->sendResetLink($credentials);
        }
    }
}
