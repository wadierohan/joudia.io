<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    protected $fillable = [
        'name', 'type', 'uniq_id', 'user_id', 'label'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function subscribers()
    {
        return $this->belongsToMany('App\Subscriber', 'field_subscriber', 'field_id', 'subscriber_id', 'uniq_id', 'id')->as('subscription')->withPivot('value')->withTimestamps();
    }
}
