<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $fillable = [
        'name', 'formation_id', 'desc', 'enabled'
    ];

    public function formation()
    {
        return $this->belongsTo('App\Formation');
    }

    public function chapters()
    {
        return $this->hasMany('App\Chapter');
    }
}
