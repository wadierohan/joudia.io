<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    protected $fillable = [
        'name', 'user_id', 'type', 'redirect', 'success_msg', 'newsletter_id', 'delay', 'uniq_id', 'html', 'css', 'js', 'visitors'
    ];

    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function subscribers()
    {
        return $this->belongsToMany('App\Subscriber')->withTimestamps();
    }

    public function newsletter()
    {
        return $this->belongsTo('App\Newsletter');
    }

    public function scopeUniqid($query, $uniq_id)
    {
        return $query->where('uniq_id', $uniq_id);
    }

    public function scopeOwned($query)
    {
        return $query->where('forms.user_id', Auth::id());
    }
}
