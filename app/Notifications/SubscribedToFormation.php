<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Formation;

class SubscribedToFormation extends Notification
{
    use Queueable;

    private $formation;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Formation $formation)
    {
        $this->formation = $formation;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = route('formateur.formation', [$this->formation->user->domain, $this->formation->url], true);
        return (new MailMessage)
            ->subject('Inscription à la formation "'.$this->formation->name.'"')
            ->greeting('Bonjour!')
            ->line('Vous avez été inscrit à la formation "'.$this->formation->name.'".')
            ->action('Formation', $url)
            ->line('Nous vous souhaitons la bienvenue');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
