<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Lang;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPasswordNotification extends Notification
{
    use Queueable;

    private $subdomain;
    private $token;
    private $firstCreation;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($subdomain, $token, $firstCreation = false)
    {
        $this->subdomain = $subdomain;
        $this->token = $token;
        $this->firstCreation = $firstCreation;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if ($this->firstCreation) {
            return (new MailMessage)
                ->subject(Lang::getFromJson('Create Password Notification'))
                ->line(Lang::getFromJson('You are receiving this email because your account was created.'))
                ->action(Lang::getFromJson('Create Password'), route('subscriber.password.reset', ['subdomain' => $this->subdomain, 'token' => $this->token], true))
                ->line(Lang::getFromJson('This password reset link will expire in :count minutes.', ['count' => config('auth.passwords.users.expire')]))
                ->line(Lang::getFromJson('Please request a new one by clicking on reset password if this one expire.'));
        } else {
            return (new MailMessage)
                ->subject(Lang::getFromJson('Reset Password Notification'))
                ->line(Lang::getFromJson('You are receiving this email because we received a password reset request for your account.'))
                ->action(Lang::getFromJson('Reset Password'), route('subscriber.password.reset', ['subdomain' => $this->subdomain, 'token' => $this->token], true))
                ->line(Lang::getFromJson('This password reset link will expire in :count minutes.', ['count' => config('auth.passwords.users.expire')]))
                ->line(Lang::getFromJson('If you did not request a password reset, no further action is required.'));
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
