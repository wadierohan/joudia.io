<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class SubscriberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'tags' => [
                'required',
                'array',
                Rule::in(auth()->user()->tags->pluck('id')),
            ],
            'formations' => [
                'array',
                Rule::in(auth()->user()->formations->pluck('id')),
            ]
        ];

        if ($this->route()->getName() === 'subscribers.store') {
            $rules['email'] = [
                'required',
                'email'
            ];
        }
        
        return $rules;
    }
}
