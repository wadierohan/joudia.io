<?php
    // format array to [{value,name},{value,name},...]
    if (! function_exists('array_format_value_name')) {
        function array_format_value_name($array, $values = ['id', 'name'], $result = [])
        {
            $value = $values[0];
            $name = $values[1];
            foreach ($array as $row) {
                $result[] = [
                    'value' => strval($row->$value),
                    'name' => strval($row->$name),
                ];
            }
            return $result;
        }
    }

    // Get File input
    if (! function_exists('get_file_input')) {
        function get_file_input()
        {
            $result = '<div class="file-container my-2">';
            $result .= Form::file('attachments[]');
            $result .= '<button type="button" class="btn btn-danger delete-file pull-right"><i class="fas fa-trash-alt"></i></button>';
            $result .= '<div class="clearfix"></div>';
            $result .= '</div>';
            return $result;
        }
    }
