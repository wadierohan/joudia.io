<?php

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;
use Auth;

class TagController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Tag::class);
    }

    public function index()
    {
        return view('tag.index');
    }

    public function create()
    {
        return view('tag.create');
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
        ];
        $request->validate($rules);
        $tag = new Tag($request->all());
        $tag->user_id = Auth::id();
        $tag->save();
        return redirect()->route('tags.index')->with('success', __('Tag créé'));
    }

    public function show(Tag $tag)
    {
        return view('tag.edit', ['tag' => $tag]);
    }

    public function edit(Tag $tag)
    {
        return view('tag.edit', ['tag' => $tag]);
    }

    public function update(Request $request, Tag $tag)
    {
        $rules = [
            'name' => 'required',
        ];
        $request->validate($rules);
        $tag->update($request->all());
        return redirect()->route('tags.index')->with('success', __('Tag mis à jour'));
    }

    public function destroy(Tag $tag)
    {
        $tag->delete();
        return redirect()->route('tags.index')->with('success', __('Tag supprimé'));
    }
}
