<?php

namespace App\Http\Controllers;

use Gate;
use App\User;
use App\Module;
use App\Chapter;
use App\Formation;
use Illuminate\Http\Request;
use App\Events\ChapterCreated;
use Illuminate\Support\Facades\Auth;
use Javascript;

class ChapterController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Chapter::class);
    }

    public function index(Formation $formation, Module $module)
    {
        return view('chapter.index', ['formation' => $formation, 'module' => $module]);
    }

    public function create(Formation $formation, Module $module)
    {
        return view('chapter.create', ['formation' => $formation, 'module' => $module]);
    }

    public function store(Formation $formation, Module $module, Request $request)
    {
        $rules = [
            'name' => 'required',
        ];
        $request->validate($rules);
        $chapter = new Chapter($request->all());
        if (!$request->has('enabled')) {
            $chapter->enabled = false;
        }
        $chapter->module_id = $module->id;
        $chapter->save();
        event(new ChapterCreated($chapter));
        return redirect()->route('chapters.wysiwyg', ['formation' => $formation, 'module' => $module, 'chapter' => $chapter])->with('success', __('Chapitre créé'));
    }

    public function show(Formation $formation, Module $module, Chapter $chapter)
    {
        return view('chapter.show', ['formation' => $formation, 'module' => $module, 'chapter' => $chapter]);
    }

    public function edit(Formation $formation, Module $module, Chapter $chapter)
    {
        return view('chapter.edit', ['formation' => $formation, 'module' => $module, 'chapter' => $chapter]);
    }

    public function update(Formation $formation, Module $module, Request $request, Chapter $chapter)
    {
        $rules = [
            'name' => 'required',
        ];
        $request->validate($rules);
        if (!$request->has('enabled')) {
            $chapter->enabled = false;
        }
        $chapter->update($request->all());
        return redirect()->route('chapters.index', ['formation' => $formation, 'module' => $module])->with('success', __('Chapitre mis à jour'));
    }

    public function destroy(Formation $formation, Module $module, Chapter $chapter)
    {
        $chapter->delete();
        return redirect()->route('chapters.index', ['formation' => $formation, 'module' => $module])->with('success', __('Chapitre supprimé'));
    }

    public function wysiwyg(Formation $formation, Module $module, Chapter $chapter)
    {
        $this->authorize('update', $chapter);

        $data = $chapter;
        $submitUrl = route('chapters.wysiwyg.update', [$formation->id, $module->id, $chapter->id]);
        $exitUrl = route('chapters.index', [$formation->id, $module->id]);
        $tags = array_format_value_name(Auth::user()->tags);
        $fieldsArray = [
            [
                'value' => '',
                'name' => __('Choisir un champs')
            ],
            [
                'value' => 'email',
                'name' => __('Email')
            ]
        ];
        $fields = array_format_value_name(Auth::user()->fields, ['uniq_id', 'name'], $fieldsArray);
        Javascript::put([
            'submitUrl' => $submitUrl,
            'exitUrl' => $exitUrl,
            'tags' => $tags,
            'fields' => $fields
        ]);
        return view('wysiwyg', compact('data'));
    }

    public function wysiwygUpdate(Formation $formation, Module $module, Request $request, Chapter $chapter)
    {
        $chapter->html = $request->html;
        $chapter->save();
        return redirect()->route('chapters.index', ['formation' => $formation, 'module' => $module])->with('success', __('Chapitre mis à jour'));
    }

    public function chapterSubscriber($subdomain, $url, Module $module, Chapter $chapter)
    {
        $user = User::subdomain($subdomain)->firstOrFail();
        $formation = $user->formations()->byUrl($url)->firstOrFail();
        if (!$module ||
            !$formation->modules()->where('modules.id', $module->id)->exists() ||
            !$chapter ||
            !$module->chapters()->where('chapters.id', $chapter->id)->exists() ||
            Gate::denies('view-formation', $formation)
        ) {
            abort(404);
        } else {
            return view('chapter.show', compact('subdomain', 'url', 'formation', 'module', 'chapter'));
        }
    }
}
