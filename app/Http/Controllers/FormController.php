<?php

namespace App\Http\Controllers;

use App\Form;
use App\Newsletter;
use Illuminate\Http\Request;
use Auth;
use App\Events\FormCreated;
use Javascript;

class FormController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Form::class);
    }

    public function index()
    {
        return view('form.index');
    }

    public function create()
    {
        return view('form.create');
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'type' => 'required|in:form,popup,survey,leadpage',
            'tags' => 'required|in:'.implode(',', Auth::user()->tags()->pluck('tags.id')->toArray()),
            'sender_id' => 'required|in:'.implode(',', Auth::user()->senders()->pluck('senders.id')->toArray()),
        ];
        $request->validate($rules);
        $form = new Form($request->all());
        if ($request->subject && $request->body) {
            $newsletter = new Newsletter();
            $newsletter->name = 'form: '.$request->name;
            $newsletter->user_id = Auth::id();
            $newsletter->subject = $request->subject;
            $newsletter->body = $request->body;
            $newsletter->type = 'welcome';
            $newsletter->sender_id = $request->sender_id;
            $newsletter->save();
            $form->newsletter_id = $newsletter->id;
        }
        $form->user_id = Auth::id();
        $form->delay = $form->delay ? $form->delay : 0;
        $form->uniq_id = uniqid('form_');
        $form->save();
        $form->tags()->sync($request->tags);
        event(new FormCreated($form));
        if (!$request->has('submit') || $request->submit === 'save') {
            return redirect()->route('forms.wysiwyg', ['form' => $form->id])->with('success', __('Formulaire créé'));
        } elseif ($request->submit === 'savenew') {
            return redirect()->route('forms.create')->with('success', __('Formulaire créé'));
        }
    }

    public function wysiwyg(Form $form)
    {
        $this->authorize('update', $form);

        $data = $form;
        $submitUrl = route('forms.wysiwyg.update', $form->id);
        $exitUrl = route('forms.index');
        $tags = array_format_value_name(Auth::user()->tags);
        $fieldsArray = [
            [
                'value' => '',
                'name' => __('Choisir un champs')
            ],
            [
                'value' => 'email',
                'name' => __('Email')
            ]
        ];
        $fields = array_format_value_name(Auth::user()->fields, ['uniq_id', 'name'], $fieldsArray);
        Javascript::put([
            'submitUrl' => $submitUrl,
            'exitUrl' => $exitUrl,
            'tags' => $tags,
            'fields' => $fields
        ]);
        return view('wysiwyg', compact('data'));
    }

    public function wysiwygUpdate(Request $request, Form $form)
    {
        $this->authorize('update', $form);
        
        $form->html = $request->html;
        $form->css = $request->css;
        $form->save();
        return redirect()->route('forms.wysiwyg', ['form' => $form->id])->with('success', __('Formulaire mis à jour'));
    }

    public function show(Form $form)
    {
        return view('form.show', ['form' => $form]);
    }

    public function edit(Form $form)
    {
        return view('form.edit', ['form' => $form]);
    }

    public function update(Request $request, Form $form)
    {
        $rules = [
            'name' => 'required',
            'type' => 'required|in:form,popup,survey,leadpage',
            'tags' => 'required|in:'.implode(',', Auth::user()->tags()->pluck('tags.id')->toArray()),
            'sender_id' => 'required|in:'.implode(',', Auth::user()->senders()->pluck('senders.id')->toArray()),
        ];
        $request->validate($rules);
        $form->update($request->all());
        if (!$form->newsletter_id) {
            if ($request->subject && $request->body) {
                $newsletter = new Newsletter();
                $newsletter->name = 'form: '.$request->name;
                $newsletter->user_id = Auth::id();
                $newsletter->subject = $request->subject;
                $newsletter->body = $request->body;
                $newsletter->type = 'welcome';
                $newsletter->sender_id = $request->sender_id;
                $newsletter->save();
                $form->newsletter_id = $newsletter->id;
            }
        } else {
            if (!$request->subject && !$request->body) {
                $form->newsletter->delete();
                $form->newsletter_id = null;
            } else {
                $form->newsletter->subject = $request->subject;
                $form->newsletter->body = $request->body;
                $form->newsletter->sender_id = $request->sender_id;
                $form->newsletter->save();
            }
        }
        $form->delay = $form->delay ? $form->delay : 0;
        $form->save();
        $form->tags()->sync($request->tags);
        return redirect()->route('forms.index')->with('success', __('Formulaire mis à jour'));
    }

    public function destroy(Form $form)
    {
        $form->delete();
        return redirect()->route('forms.index')->with('success', __('Formulaire supprimé'));
    }
}
