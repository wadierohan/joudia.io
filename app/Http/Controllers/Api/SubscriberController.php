<?php

namespace App\Http\Controllers\Api;

use App\Subscriber;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubscriberController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Subscriber::class);
    }

    public function search(Request $request)
    {
        $perPage = $request->perPage ?? 10;
        $subscribers = Subscriber::owned()->with(['ownedTags', 'ownedForms', 'ownedFormations']);

        // Emails
        foreach ($request->filter['email'] as $email) {
            if ($email['option'] === 'has') {
                $subscribers->where('email', 'like', '%'.$email['value'].'%');
            } elseif ($email['option'] === 'startwith') {
                $subscribers->where('email', 'like', $email['value'].'%');
            } elseif ($email['option'] === 'endwith') {
                $subscribers->where('email', 'like', '%'.$email['value']);
            } elseif ($email['option'] === 'equalto') {
                $subscribers->where('email', $email['value']);
            }
        }

        // Tags
        foreach ($request->filter['tag'] as $tag) {
            if ($tag['option'] === 'has') {
                $subscribers->hasTag($tag['value']);
            } elseif ($tag['option'] === 'hasnt') {
                $subscribers->hasntTag($tag['value']);
            }
        }

        // Forms
        foreach ($request->filter['form'] as $form) {
            if ($form['option'] === 'has') {
                $subscribers->hasForm($form['value']);
            } elseif ($form['option'] === 'hasnt') {
                $subscribers->hasntForm($form['value']);
            }
        }

        // Formations
        foreach ($request->filter['formation'] as $formation) {
            if ($formation['option'] === 'has') {
                $subscribers->hasFormation($formation['value']);
            } elseif ($formation['option'] === 'hasnt') {
                $subscribers->hasntFormation($formation['value']);
            }
        }

        $subscribers = $subscribers->paginate($perPage);
        return response()->json($subscribers);
    }
}
