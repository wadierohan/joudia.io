<?php

namespace App\Http\Controllers\Api;

use App\Newsletter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class NewsletterController extends Controller
{
    public function get_file_input()
    {
        return get_file_input();
    }

    public function delete_newsletter_file(Request $request)
    {
        $stat = false;
        $path = $request->filename;
        list($newsletter_id, $filename) = explode('/', $path);
        $newsletter = Newsletter::find($newsletter_id);
        if (auth()->user()->can('delete', $newsletter)) {
            $stat = Storage::disk('newsletters')->delete($path);
        }
        return response()->json(['stat' => $stat, 'msg' => $stat ? __('Fichier supprimé.') : __('Une erreur est survenue.')]);
    }
}
