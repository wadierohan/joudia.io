<?php

namespace App\Http\Controllers\Api;

use App\Queue;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QueueController extends Controller
{
    public function opened(Queue $queue)
    {
        $queue->opened = 1;
        $queue->opened_at = Carbon::now();
        $queue->save();
    }
}
