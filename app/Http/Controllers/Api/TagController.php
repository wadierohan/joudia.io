<?php

namespace App\Http\Controllers\Api;

use App\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TagController extends Controller
{
    public function create(Request $request)
    {
        $rules = [
            'name' => 'required',
        ];
        $request->validate($rules);
        $tag = new Tag($request->all());
        $tag->user_id = Auth::id();
        $tag->save();
        return redirect()->json(['success' => __('Tag créé')]);
    }

    public function index(Request $request)
    {
        return response()->json($request->user()->tags);
    }
}
