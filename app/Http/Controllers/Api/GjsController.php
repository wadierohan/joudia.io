<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class GjsController extends Controller
{
    public function uploaduserimage(Request $request)
    {
        $newImages = [];
        foreach ($request->file('files') as $image) {
            $filename = $image->store($request->userid.'/img');
            $data = [
                'src' => asset('storage/'.$request->userid.'/img/'.$filename),
            ];
            array_push($newImages, $data);
        }
        return response()->json([
            'data' => $newImages,
        ]);
    }

    public function storegjs(Request $request)
    {
        $userid = $request->userid;
        $json = json_encode($request->except('userid'));
        File::put(asset('storage/'.$userid.'/gjs/json.json'), $json);
        return response()->json([
            'success' => true,
        ]);
    }

    public function loadgjs($userid)
    {
        $json = File::get(asset('storage/'.$userid.'/gjs/json.json'));
        return response($json)->header('Content-Type', 'application/json');
    }
}
