<?php

namespace App\Http\Controllers\Api;

use App\Form;
use App\Events\FormSubmited;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FormController extends Controller
{
    public function getForm(Request $request)
    {
        $rules = [
            'uniqid' => 'required',
        ];
        $request->validate($rules);
        $form = Form::uniqid($request->uniqid)->firstOrFail();
        return response()->json(['html' => $form->html, 'css' => $form->css, 'js' => $form->js, 'isPopup' => $form->type == 'popup' ? 1 : '']);
    }

    public function submitForm(Request $request)
    {
        $rules = [
            'uniqid' => 'required',
            'tag' => 'required',
            'email' => 'required|email',
        ];
        $request->validate($rules);
        $form = Form::uniqid($request->uniqid)->firstOrFail();
        event(new FormSubmited($request));
        return response()->json(array('success_msg' => $form->success_msg ?? '', 'redirect' => $form->redirect ?? '', 'isPopup' => $form->type == 'popup' ? 1 : ''));
    }

    public function index(Request $request)
    {
        return response()->json($request->user()->forms);
    }
}
