<?php

namespace App\Http\Controllers\AuthSubscriber;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm($subdomain)
    {
        return view('authSubscriber.login', compact('subdomain'));
    }
    
    /**
     * Where to redirect subscriber after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:subscriber')->except('logout');
    }

    protected function guard()
    {
        return auth()->guard('subscriber');
    }
}
