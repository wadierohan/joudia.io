<?php

namespace App\Http\Controllers;

use App\Schedule;
use Illuminate\Http\Request;
use Auth;

class ScheduleController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Schedule::class);
    }

    public function index()
    {
        return view('schedule.index');
    }

    public function create()
    {
        return view('schedule.create');
    }

    public function store(Request $request)
    {
        $rules = [
            'newsletter_id' => 'required|in:'.implode(',', Auth::user()->newsletters()->pluck('newsletters.id')->toArray()),
            'tags_type' => 'required|in:alltags,all,once',
            'tags' => 'required_unless:tags_type,alltags',
            'queued_at' => 'required|date_format:"Y-m-d H:i:s"',
        ];
        $request->validate($rules);
        $schedule = new Schedule($request->all());
        $schedule->user_id = Auth::id();
        $schedule->save();
        $schedule->tags()->sync($request->tags);
        return redirect()->route('schedules.index')->with('success', __('Plannification créée'));
    }

    public function show(Schedule $schedule)
    {
        return view('schedule.show', ['schedule' => $schedule]);
    }

    public function edit(Schedule $schedule)
    {
        return view('schedule.edit', ['schedule' => $schedule]);
    }

    public function update(Request $request, Schedule $schedule)
    {
        $rules = [
            'newsletter_id' => 'required|in:'.implode(',', Auth::user()->newsletters()->pluck('newsletters.id')->toArray()),
            'tags_type' => 'required|in:alltags,all,once',
            'tags' => 'required_unless:tags_type,alltags',
            'queued_at' => 'required|date_format:"Y-m-d H:i:s"',
        ];
        $request->validate($rules);
        $schedule->update($request->all());
        $schedule->tags()->sync($request->tags);
        return redirect()->route('schedules.index')->with('success', __('Plannification mise à jour'));
    }

    public function destroy(Schedule $schedule)
    {
        $schedule->delete();
        return redirect()->route('schedules.index')->with('success', __('Plannification supprimée'));
    }
}
