<?php

namespace App\Http\Controllers;

use App\Formation;
use App\Subscriber;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Gate;
use App\Events\SubscriberAddedToFormation;

class FormationController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Formation::class);
    }

    public function index()
    {
        return view('formation.index');
    }

    public function create()
    {
        $subscribers = Subscriber::owned()->get();
        return view('formation.create', ['subscribers' => $subscribers]);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'url' => 'required|unique:formations,url,NULL,id,user_id,'.Auth::id(),
            'img' => 'image|max:2048',
        ];
        $request->validate($rules);
        $formation = new Formation($request->except('img'));
        $formation->user_id = Auth::id();
        if ($request->hasFile('img')) {
            $request->img->store('public/'.Auth::id().'/images');
            $formation->img = $request->img->hashName();
        }
        if (!$request->has('enabled')) {
            $formation->enabled = false;
        }
        $formation->save();
        $new_users = $formation->subscribers()->sync($request->subscribers);
        $new_subscribed_account = Subscriber::whereIn('id', $new_users['attached'])->get();
        foreach ($new_subscribed_account as $subscriber) {
            event(new SubscriberAddedToFormation($subscriber, $formation));
        }
        return redirect()->route('formations.index')->with('success', __('Formation créée'));
    }

    public function show(Formation $formation)
    {
        return view('formation.show', ['formation' => $formation]);
    }

    public function edit(Formation $formation)
    {
        $subscribers = Subscriber::owned()->get();
        return view('formation.edit', ['formation' => $formation, 'subscribers' => $subscribers]);
    }

    public function update(Request $request, Formation $formation)
    {
        $rules = [
            'name' => 'required',
            'url' => 'required|unique:formations,url,'.$formation->id.',id,user_id,'.Auth::id(),
            'img' => 'image|max:2048',
        ];
        $request->validate($rules);
        if ($request->hasFile('img')) {
            $request->img->store('public/'.Auth::id().'/images');
            $formation->img = $request->img->hashName();
        }
        if (!$request->has('enabled')) {
            $formation->enabled = false;
        }
        $formation->update($request->except('img'));
        $new_users = $formation->subscribers()->sync($request->subscribers);
        $new_subscribed_account = Subscriber::whereIn('id', $new_users['attached'])->get();
        foreach ($new_subscribed_account as $subscriber) {
            event(new SubscriberAddedToFormation($subscriber, $formation));
        }
        return redirect()->route('formations.index')->with('success', __('Formation créée'));
    }

    public function destroy(Formation $formation)
    {
        $formation->delete();
        return redirect()->route('formations.index')->with('success', __('Formulaire supprimée'));
    }

    public function formationSubscriber($subdomain, $url)
    {
        $user = User::subdomain($subdomain)->firstOrFail();
        $formation = $user->formations()->byUrl($url)->firstOrFail();
        if (Gate::allows('view-formation', $formation)) {
            return view('formation.show', ['formation' => $formation, 'subdomain' => $subdomain, 'url' => $url]);
        } else {
            abort(404);
        }
    }
}
