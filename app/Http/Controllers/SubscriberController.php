<?php

namespace App\Http\Controllers;

use Auth;
use App\Subscriber;
use Illuminate\Http\Request;
use App\Http\Requests\SubscriberRequest;
use App\Formation;
use App\Events\SubscriberAddedToFormation;

class SubscriberController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Subscriber::class);
    }

    public function index()
    {
        $subscribers = Subscriber::owned()->get();
        return view('subscriber.index', ['subscribers' => $subscribers]);
    }

    public function create()
    {
        $fields = auth()->user()->fields;
        $tags = auth()->user()->tags;
        $formations = auth()->user()->formations;
        return view('subscriber.create', compact('fields', 'tags', 'formations'));
    }

    public function store(SubscriberRequest $request)
    {
        $subscriber = Subscriber::where('email', $request->email)->first();

        if ($subscriber && $subscriber->isOwned()) {
            return redirect()->back()->withInput()->with('warning', __('Abonné existe déjà'));
        }
        if (!$subscriber) {
            $subscriber = Subscriber::create(['email' => $request->email]);
        }

        $attachFields = [];
        $fields = $request->only(auth()->user()->fields->pluck('uniq_id')->toArray());
        foreach ($fields as $field => $value) {
            $attachFields[$field] = ['value' => $value];
        }
        $subscriber->fields()->attach($attachFields);
        
        $subscriber->tags()->attach($request->tags);
        $subscriber->formations()->attach($request->formations);

        $formations = $request->formations ?? [];

        foreach ($formations as $formation_id) {
            $formation = Formation::find($formation_id);
            event(new SubscriberAddedToFormation($subscriber, $formation));
        }

        return redirect()->route('subscribers.index')->with('success', __('Abonné créé'));
    }

    public function show(Subscriber $subscriber)
    {
        //
    }

    public function edit(Subscriber $subscriber)
    {
        $fields = auth()->user()->fields;
        $tags = auth()->user()->tags;
        $formations = auth()->user()->formations;
        return view('subscriber.edit', compact('subscriber', 'tags', 'fields', 'formations'));
    }

    public function update(SubscriberRequest $request, Subscriber $subscriber)
    {
        $attachFields = [];
        $fields = $request->only(auth()->user()->fields->pluck('uniq_id')->toArray());
        foreach ($fields as $field => $value) {
            $attachFields[$field] = ['value' => $value];
        }
        $subscriber->fields()->syncWithoutDetaching($attachFields);
        $subscriber->tags()->sync($request->tags);
        $new_formations = $subscriber->formations()->sync($request->formations);
        
        foreach ($new_formations['attached'] as $formation_id) {
            $formation = Formation::find($formation_id);
            event(new SubscriberAddedToFormation($subscriber, $formation));
        }

        return redirect()->route('subscribers.index')->with('success', __('Abonné mis à jour'));
    }

    public function destroy(Subscriber $subscriber)
    {
        $tags = Auth::user()->tags->pluck('id');
        $formations = Auth::user()->formations->pluck('id');
        $fields = Auth::user()->fields->pluck('id');

        $subscriber->fields()->wherePivotIn('field_id', $fields)->detach();
        
        $subscriber->formations()->detach($formations);
        $subscriber->tags()->detach($tags);

        return redirect()->route('subscribers.index')->with('success', __('Abonné supprimé'));
    }
}
