<?php

namespace App\Http\Controllers;

use App\Sender;
use Illuminate\Http\Request;
use Auth;

class SenderController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Sender::class);
    }

    public function index()
    {
        return view('sender.index');
    }

    public function create()
    {
        return view('sender.create');
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'from_name' => 'required',
            'from_email' => 'required|email',
            'reply_name' => 'required',
            'reply_email' => 'required|email',
            'mailserver_id' => 'required|in:'.implode(',', Auth::user()->mailservers()->pluck('mailservers.id')->toArray()),
        ];
        $request->validate($rules);
        $sender = new Sender($request->all());
        $sender->user_id = Auth::id();
        $sender->save();
        return redirect()->route('senders.index')->with('success', __('Expéditeur créé'));
    }

    public function show(Sender $sender)
    {
        return view('sender.edit', ['sender' => $sender]);
    }

    public function edit(Sender $sender)
    {
        return view('sender.edit', ['sender' => $sender]);
    }

    public function update(Request $request, Sender $sender)
    {
        $rules = [
            'name' => 'required',
            'from_name' => 'required',
            'from_email' => 'required|email',
            'reply_name' => 'required',
            'reply_email' => 'required|email',
            'mailserver_id' => 'required|in:'.implode(',', Auth::user()->mailservers()->pluck('mailservers.id')->toArray()),
        ];
        $request->validate($rules);
        $sender->update($request->all());
        return redirect()->route('senders.index')->with('success', __('Expéditeur mis à jour'));
    }

    public function destroy(Sender $sender)
    {
        $sender->delete();
        return redirect()->route('senders.index')->with('success', __('Expéditeur supprimé'));
    }
}
