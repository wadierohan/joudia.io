<?php

namespace App\Http\Controllers;

use App\Mailserver;
use Illuminate\Http\Request;
use Auth;

class MailserverController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Mailserver::class);
    }

    public function index()
    {
        return view('mailserver.index');
    }

    public function create()
    {
        return view('mailserver.create');
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'host' => 'required',
            'port' => 'required|integer',
            'encryption' => 'in:,ssl,tls',
        ];
        $request->validate($rules);
        $mailserver = new Mailserver($request->all());
        $mailserver->user_id = Auth::id();
        $mailserver->save();
        return redirect()->route('mailservers.index')->with('success', __('Serveur mail créé'));
    }

    public function show(Mailserver $mailserver)
    {
        return view('mailserver.edit', ['mailserver' => $mailserver]);
    }

    public function edit(Mailserver $mailserver)
    {
        return view('mailserver.edit', ['mailserver' => $mailserver]);
    }

    public function update(Request $request, Mailserver $mailserver)
    {
        $rules = [
            'name' => 'required',
            'host' => 'required',
            'port' => 'required|integer',
            'encryption' => 'in:,ssl,tls',
        ];
        $request->validate($rules);
        $mailserver->update($request->password ? $request->all() : $request->except('password'));
        return redirect()->route('mailservers.index')->with('success', __('Serveur mail mis à jour'));
    }

    public function destroy(Mailserver $mailserver)
    {
        $mailserver->delete();
        return redirect()->route('mailservers.index')->with('success', __('Serveur mail supprimé'));
    }
}
