<?php

namespace App\Http\Controllers;

use App\Module;
use App\Formation;
use App\User;
use Illuminate\Http\Request;
use Gate;

class ModuleController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Module::class);
    }

    public function index(Formation $formation)
    {
        return view('module.index', ['formation' => $formation]);
    }

    public function create(Formation $formation)
    {
        return view('module.create', ['formation' => $formation]);
    }

    public function store(Formation $formation, Request $request)
    {
        $rules = [
            'name' => 'required',
        ];
        $request->validate($rules);
        $module = new Module($request->all());
        if (!$request->has('enabled')) {
            $module->enabled = false;
        }
        $module->formation_id = $formation->id;
        $module->save();
        return redirect()->route('modules.index', ['formation' => $formation])->with('success', __('Module créé'));
    }

    public function show(Formation $formation, Module $module)
    {
        return view('module.show', ['formation' => $formation, 'module' => $module]);
    }

    public function edit(Formation $formation, Module $module)
    {
        return view('module.edit', ['formation' => $formation, 'module' => $module]);
    }

    public function update(Formation $formation, Request $request, Module $module)
    {
        $rules = [
            'name' => 'required',
        ];
        $request->validate($rules);
        if (!$request->has('enabled')) {
            $module->enabled = false;
        }
        $module->update($request->all());
        return redirect()->route('modules.index', ['formation' => $formation])->with('success', __('Module mis à jour'));
    }

    public function destroy(Formation $formation, Module $module)
    {
        $module->delete();
        return redirect()->route('modules.index', ['formation' => $formation])->with('success', __('Module supprimé'));
    }

    public function moduleSubscriber($subdomain, $url, Module $module)
    {
        $user = User::subdomain($subdomain)->firstOrFail();
        $formation = $user->formations()->byUrl($url)->firstOrFail();
        if (!$module || !$formation->modules()->where('modules.id', $module->id)->exists() || Gate::denies('view-formation', $formation)) {
            abort(404);
        } else {
            return view('module.show', compact('subdomain', 'url', 'formation', 'module'));
        }
    }
}
