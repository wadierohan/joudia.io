<?php

namespace App\Http\Controllers;

use App\Field;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FieldController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Field::class);
    }

    public function index()
    {
        return view('field.index');
    }

    public function create()
    {
        return view('field.create');
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|not_regex:/^\s*email\s*$/i',
        ];
        $request->validate($rules);
        $field = new Field($request->all());
        $field->uniq_id = uniqid('field_');
        $field->user_id = Auth::id();
        $field->save();
        return redirect()->route('fields.index')->with('success', __('Champs créé'));
    }

    public function show(Field $field)
    {
        return view('field.show', ['field' => $field]);
    }

    public function edit(Field $field)
    {
        return view('field.edit', ['field' => $field]);
    }

    public function update(Request $request, Field $field)
    {
        $rules = [
            'name' => 'required|not_regex:/^\s*email\s*$/i',
        ];
        $request->validate($rules);
        $field->update($request->all());
        return redirect()->route('fields.index')->with('success', __('Champs mis à jour'));
    }

    public function destroy(Field $field)
    {
        $field->delete();
        return redirect()->route('fields.index')->with('success', __('Champs supprimé'));
    }
}
