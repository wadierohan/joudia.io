<?php

namespace App\Http\Controllers;

use App\Campaign;
use App\CampaignStep;
use Illuminate\Http\Request;
use Auth;

class CampaignController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Campaign::class);
    }

    public function index()
    {
        return view('campaign.index');
    }

    public function create()
    {
        return view('campaign.create');
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'trigger' => 'required|in:at_date,new_form_sub',
            'trigger_value' => 'required',
            'tags_type' => 'required_if:trigger,at_date|in:alltags,all,once',
        ];
        $request->validate($rules);
        $campaign = new Campaign($request->all());
        $campaign->user_id = Auth::id();
        $campaign->save();
        $campaign->tags()->sync($request->tags);
        return redirect()->route('campaigns.automate', $campaign->id)->with('success', __('Campagne créée'));
    }

    public function show(Campaign $campaign)
    {
        //
    }

    public function edit(Campaign $campaign)
    {
        return view('campaign.edit', ['campaign' => $campaign]);
    }

    public function update(Request $request, Campaign $campaign)
    {
        $rules = [
            'name' => 'required',
            'trigger' => 'required|in:at_date,new_form_sub',
            'trigger_value' => 'required',
            'tags_type' => 'required_if:trigger,at_date|in:alltags,all,once',
        ];
        $request->validate($rules);
        $campaign->update($request->all());
        $campaign->tags()->sync($request->tags);
        return redirect()->route('campaigns.index')->with('success', __('Campagne mise à jour'));
    }

    public function destroy(Campaign $campaign)
    {
        $campaign->delete();
        return redirect()->route('campaigns.index')->with('success', __('Campagne supprimée'));
    }

    public function automate(Campaign $campaign)
    {
        return view('campaign.automate', ['campaign' => $campaign]);
    }

    public function automateUpdate(Request $request, Campaign $campaign)
    {
        $newsletters = $request->step['newsletter_id'];
        $delay = $request->step['delay'];
        foreach ($newsletters as $key => $newsletter) {
            $campaign_step = CampaignStep::updateOrCreate(
                ['order' => $key, 'campaign_id' => $campaign->id],
                ['newsletter_id' => $newsletter, 'delay' => $delay[$key] ? $delay[$key] : 0]
            );
        }
        return redirect()->route('campaigns.index')->with('success', __('Etapes de Campagne mises à jour'));
    }
}
