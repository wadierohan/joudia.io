<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        return view('dashboard');
    }

    public function formateurIndex($subdomain)
    {
        $user = User::with('formations')->subdomain($subdomain)->firstOrFail();
        return view('formateur.show', compact('user'));
    }
}
