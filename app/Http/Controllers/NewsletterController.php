<?php

namespace App\Http\Controllers;

use Auth;
use App\Newsletter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class NewsletterController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Newsletter::class);
    }

    public function index()
    {
        return view('newsletter.index');
    }

    public function create()
    {
        return view('newsletter.create');
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'subject' => 'required',
            'body' => 'required',
            'attachments.*' => 'required|file|mimes:jpg,jpeg,png,bmp,doc,docx,pdf,xls,xlsx,ppt,txt|max:5000',
        ];
        $request->validate($rules);

        $newsletter = new Newsletter($request->except('attachments'));
        $newsletter->user_id = Auth::id();
        $newsletter->save();

        if (isset($request->attachments) && $request->attachments) {
            foreach ($request->attachments as $file) {
                $file->storeAs($newsletter->id, uniqid().'_'.$file->getClientOriginalName(), 'newsletters');
            }
        }
        return redirect()->route('newsletters.index')->with('success', __('Newsletter créée'));
    }

    public function show(Newsletter $newsletter)
    {
        $files = Storage::disk('newsletters')->files($newsletter->id);
        return view('newsletter.edit', compact('newsletter', 'files'));
    }

    public function edit(Newsletter $newsletter)
    {
        $files = Storage::disk('newsletters')->files($newsletter->id);
        return view('newsletter.edit', compact('newsletter', 'files'));
    }

    public function update(Request $request, Newsletter $newsletter)
    {
        $rules = [
            'name' => 'required',
            'subject' => 'required',
            'body' => 'required',
            'attachments.*' => 'required|file|mimes:jpg,jpeg,png,bmp,doc,docx,pdf,xls,xlsx,ppt,txt|max:5000',
        ];
        $request->validate($rules);
        $newsletter->update($request->except('attachments'));

        if (isset($request->attachments) && $request->attachments) {
            foreach ($request->attachments as $file) {
                $file->storeAs($newsletter->id, uniqid().'_'.$file->getClientOriginalName(), 'newsletters');
            }
        }

        return redirect()->route('newsletters.index')->with('success', __('Newsletter mise à jour'));
    }

    public function destroy(Newsletter $newsletter)
    {
        Storage::disk('newsletters')->deleteDirectory($newsletter->id);
        $newsletter->delete();
        return redirect()->route('newsletters.index')->with('success', __('Newsletter supprimée'));
    }
}
