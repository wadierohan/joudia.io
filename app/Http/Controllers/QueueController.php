<?php

namespace App\Http\Controllers;

use App\Queue;
use Carbon\Carbon;
use App\Schedule;
use App\Subscriber;
use App\Newsletter;
use Auth;

class QueueController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Queue::class);
    }

    public function index()
    {
        $queues = Auth::user()->queues;
        return view('queue.index', ['queues' => $queues]);
    }

    public function showSchedule(Schedule $schedule)
    {
        $queues = $schedule->queues;
        return view('queue.index', ['queues' => $queues]);
    }

    public function showNewsletter(Newsletter $newsletter)
    {
        $queues = $newsletter->queues;
        return view('queue.index', ['queues' => $queues]);
    }

    public function showSubscriber(Subscriber $subscriber)
    {
        $queues = $subscriber->queues()->owned()->get();
        return view('queue.index', ['queues' => $queues]);
    }

    public function destroy(Queue $queue)
    {
        $queue->delete();
        return redirect()->back()->with('success', __('Queue supprimée'));
    }
}
