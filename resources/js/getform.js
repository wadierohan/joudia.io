let getNodes = str => new DOMParser().parseFromString(str, 'text/html').body.childNodes;

let thisScript = document.currentScript;

let initJoudiaFunctions = function () {
    let functionScriptSrc = thisScript.src.replace('getform', 'formFunctions');
    if (document.querySelector(`script[src="${functionScriptSrc}"]`) === null) {
        let scriptTag = document.createElement("script");
        scriptTag.src = functionScriptSrc;
        scriptTag.onreadystatechange= function () {
//This is for IE
            if (this.readyState == 'complete') {
                initJoudiaFunctions(); };
        }
        scriptTag.onload = initJoudiaFunctions;
        document.getElementsByTagName("body")[0].appendChild(scriptTag);
    } else {
        let uniqid = thisScript.dataset.uniqid;
        let url = thisScript.src.replace('js/getform.js', 'api/forms/getForm');
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                let result = JSON.parse(this.responseText);

                let tag = document.createElement("style");
                let t = document.createTextNode(result.css);
                tag.appendChild(t);
                document.getElementsByTagName("head")[0].appendChild(tag);
                console.log(result.isPopup)
                if (!result.isPopup) {
                    var containerTag = document.createElement("section");
                    containerTag.dataset.uniqid = uniqid;
                    containerTag.classList.add("joudiaContainer");
                } else {
                    var containerTag = document.createElement("div");
                    containerTag.dataset.uniqid = uniqid;
                    containerTag.classList.add("joudiaContainer");
                    containerTag.classList.add("joudia-modal");
                    let modalHeader = document.createElement("div")
                    modalHeader.classList.add("joudia-modal-header")
                    let closeBtn = document.createElement("span")
                    closeBtn.classList.add("joudia-close-modal");
                    closeBtn.innerHTML = '&times;'
                    closeBtn.style.float = 'right'
                    closeBtn.style.cursor = 'pointer'
                    closeBtn.style.padding = '5px'
                    closeBtn.onclick = function(){
                        closeBtn.closest('.joudiaContainer').style.display = 'none'
                    }
                    modalHeader.style.padding = '10px'
                    modalHeader.style.display = 'flow-root'

                    modalHeader.appendChild(closeBtn)
                    containerTag.appendChild(modalHeader)
                }

                const containerHtmlNodes = Array.prototype.slice.call(getNodes(result.html));
                for (let i = 0; i < containerHtmlNodes.length; i++) {
                    containerTag.appendChild(containerHtmlNodes[i]);
                }
                thisScript.replaceWith(containerTag);
                if (result.isPopup) {
                    showModal(containerTag)
                }
            }
        };
        xhttp.open("POST", url, true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        //xhttp.setRequestHeader("Access-Control-Allow-Methods", "POST");
        //xhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhttp.send("uniqid="+uniqid);
    }
}
initJoudiaFunctions();
