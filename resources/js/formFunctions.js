let validateEmail = function (email) {
    let re = /^(([^<>()\[\]\.,;:\s@"]+(\.[^<>()\[\]\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

let getElementsValue = function (elements, tag, uniqid) {
    let obj ={};
    for (let i = 0; i < elements.length; i++) {
        let item = elements.item(i);
        if (item.name.length > 0) {
            obj[item.name] = item.value;
        }
    }
    let array = [];
    array.push("tag="+tag);
    array.push("uniqid="+uniqid);
    for (let prop in obj) {
        array.push(prop + "=" + obj[prop]);
    }
    return array.join("&");
}

let showErrors = function (form, result) {
    let joudiaValidationError = form.querySelector('.joudiaValidationError');
    if (joudiaValidationError !== null) {
        joudiaValidationError.parentNode.removeChild(joudiaValidationError);
    }
    let container = document.createElement("ul");
    container.classList.add("joudiaValidationError");
    container.style.border = "1px solid #ebccd1";
    container.style.borderRadius = "4px";
    container.style.paddingTop = "5px";
    container.style.paddingBottom = "5px";
    container.style.color = "#a94442";
    container.style.backgroundColor = "#f2dede";
    container.style.setProperty("-webkit-transition", "1s ease-in-out");
    container.style.setProperty("-moz-transition", "1s ease-in-out");
    container.style.setProperty("-o-transition", "1s ease-in-out");
    container.style.transition = "1s ease-in-out";
    container.style.opacity = "0";
    for (let field in result.errors) {
        let fieldContainer = document.createElement("li");
        let fieldTitle = document.createTextNode(field);
        fieldContainer.appendChild(fieldTitle);

        if (result.errors.hasOwnProperty(field)) {
            let errors = result.errors[field];
            let errorsContainer = document.createElement("ul");
            errors.forEach(function (error) {
                let errorContainer = document.createElement("li");
                let errorTitle = document.createTextNode(error);
                errorContainer.appendChild(errorTitle);
                errorsContainer.appendChild(errorContainer);
            });
            fieldContainer.appendChild(errorsContainer);
        }
        container.appendChild(fieldContainer);
    }
    form.prepend(container);
    setTimeout(function () {
        container.style.opacity = "1"; }, 300);
}

showModal = function (form) {
    form.style.dislpay = "block"
    form.style.position = "fixed"
    form.style.top = "50%"
    form.style.left = "50%"
    form.style.setProperty("-ms-transform", "translate(-50%, -50%)")
    form.style.setProperty("transform", "translate(-50%, -50%)")
    form.style.border = "1px solid #ccc"
    form.style.borderRadius = "4px"

}

submitJoudiaForm = function (form,e) {
    e.preventDefault();
    let tag = form.dataset.tag;
    let url = form.action;
    let uniqid = form.closest('.joudiaContainer').dataset.uniqid;
    let fields = getElementsValue(form.elements, tag, uniqid);
    let email = form.elements['email'].value;
    let submitButton = form.querySelector('button[type="submit"]');
    let savedSubmitButton = submitButton.innerHTML;
    if (validateEmail(email)) {
        submitButton.disabled = true;
        submitButton.innerHTML = "Veuillez patienter..";
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4) {
                if (this.status == 200) {
                    let result = JSON.parse(this.responseText);
                    if (result.redirect != "") {
                        setTimeout(function () {
                            document.location.href=result.redirect;
                        }, 5000);
                    } else if (result.success_msg != "") {
                        form.innerHTML = result.success_msg;
                    }
                    if (result.isPopup) {
                        setTimeout(function () {
                            form.closest('.joudiaContainer').style.display = "none";
                        }, 5000);
                    }
                    if (result.success_msg == "" && result.redirect == "") {
                        form.innerHTML = "Vous vous êtes inscrit avec succès";
                    }
                } else if (this.status == 422) {
                    showErrors(form, JSON.parse(this.responseText))
                    submitButton.disabled = false;
                    submitButton.innerHTML = savedSubmitButton;
                } else {
                    submitButton.innerHTML = "Oops! Une erreur est survenue lors de l\'inscription.";
                }
            }
        };
        xhttp.open("POST", url, true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        //xhttp.setRequestHeader("Access-Control-Allow-Methods", "POST");
        //xhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhttp.send(fields);
    } else {
        alert("Veuillez saisir une adresse email valide.");
    }
};
