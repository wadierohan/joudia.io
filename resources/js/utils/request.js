import axios from 'axios'

const token = document.head.querySelector('meta[name="csrf-token"]')
const baseUrl = document.head.querySelector('meta[name="base-url"]')

if (!token || !baseUrl) {
    console.error('CSRF token or baseUrl not found: CSRF: ' + token + ' - baseUrl : ' + baseUrl)
}
// create an axios instance
const request = axios.create({
    baseURL: baseUrl.content,
    timeout: 5000,
    headers: { 
        'X-Requested-With': 'XMLHttpRequest',
        'X-CSRF-TOKEN' : token.content
    }
})

// request interceptor
request.interceptors.request.use(
    config => config,
    error => {
        console.log(error) // for debug
        Promise.reject(error)
    }
)

// response interceptor
request.interceptors.response.use(
    response => response,
    error => {
        console.log(error) // for debug
        Promise.reject(error)
    }
)

export default request
