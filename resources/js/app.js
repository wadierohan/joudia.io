import './bootstrap';

window.tinymce = require('tinymce');
window.DataTable = require('datatables.net-bs4');
window.toastr = require('toastr');
window.ClipboardJS = require('clipboard');
window.sweetalert = require('sweetalert');
window.datetimepicker = require('jquery-datetimepicker');
window.select2 = require('select2');
window.grapesjs = require('grapesjs');

import grapesjsJoudia from 'grapesjs-preset-joudia'

import './jquery-duration-picker';
import './custom';
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import Vue from 'vue'
import store from './store'
import Paginate from 'vuejs-paginate'

Vue.component('paginate', Paginate)
Vue.component('table-subscribers', require('./tables/Subscribers.vue').default);

const app = new Vue({
    el: '#app',
    store
});

