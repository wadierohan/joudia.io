$(function () {
    $('.delete-submit-form').click(function () {
        var that = $(this);
        swal({
            title: "Attention!",
            text: "Êtes vous sûr de vouloir supprimer cela?",
            icon: "warning",
            buttons: ["Non", "Oui"],
            dangerMode: true,
        }).then((confirmed) => {
            if (confirmed) {
                that.next('form').submit();
            } else {
                return;
            }
        });
    })

    $('.datatable').DataTable({
        "autoWidth": false,
        "language": {
            "sProcessing":     "Traitement en cours...",
            "sSearch":         "Rechercher ",
            "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
            "sInfo":           "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            "sInfoEmpty":      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            "sInfoPostFix":    "",
            "sLoadingRecords": "Chargement en cours...",
            "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            "sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
            "oPaginate": {
                "sFirst":      "Premier",
                "sPrevious":   "Pr&eacute;c&eacute;dent",
                "sNext":       "Suivant",
                "sLast":       "Dernier"
            },
            "oAria": {
                "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
                "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
            },
            "select": {
                "rows": {
                    _: "%d lignes séléctionnées",
                    0: "Aucune ligne séléctionnée",
                    1: "1 ligne séléctionnée"
                }
            }
        }
    });

    var clipboard = new ClipboardJS('.copy-clipboard');

    $('.copy-clipboard').tooltip({
        title: 'Copié',
        trigger: 'manual',
    });

    function hideTooltip(e)
    {
        e.tooltip('hide');
    }

    clipboard.on('success', function (e) {
        var that = $(e.trigger);
        that.tooltip('show');
        setTimeout(function () {
            that.tooltip('hide');
        }, 2000);
    });

    $.datetimepicker.setLocale('fr');
    $('.datetimepicker').datetimepicker({
        step: 5,
        format:'Y-m-d H:i:00',
        minDate:0,
    });

    $.timeDurationPicker.langs.fr_FR = {
        years: "Années",
        months: "Mois",
        days: "Jours",
        hours: "Heures",
        minutes: "Minutes",
        seconds: "Secondes",
        and: "et",
        button_ok: "Valider",
        units: {
            year: {
                one: "année",
                few: "années",
                many: "années"
            },
            month: {
                one: "mois",
                few: "mois",
                many: "mois"
            },
            day: {
                one: "jour",
                few: "jours",
                many: "jours"
            },
            hour: {
                one: "heure",
                few: "heures",
                many: "heures"
            },
            minute: {
                one: "minute",
                few: "minutes",
                many: "minutes"
            },
            second: {
                one: "seconde",
                few: "secondes",
                many: "secondes"
            }
        }
    }

    $.timeDurationPicker.i18n.pluralRules.fr_FR = function ( count ) {
        count = parseInt(count);
        var m10 = count % 10;
        var m100 = count % 100;

        if ( m10 === 1 && m100 !== 11 ) {
            return "one";
        } else if ( ( m10 >= 2 && m10 <= 4 ) && ( m100 < 12 || m100 > 14 ) ) {
            return "few";
        } else if ( m10 === 0 || ( m10 >= 5 && m10 <= 9 ) || ( m100 >= 11 && m100 <= 14 ) ) {
            return "many";
        } else {
            return "other";
        }
    }

    $('.durationpicker').timeDurationPicker({
        lang: 'fr_FR',
        defaultValue: function (element) {
            return element.siblings('.delay').val();
        },
        onSelect: function (element, seconds, duration) {
            element.siblings('.delay').val(seconds);
            element.val(duration);
        },
        years: false,
        months: false,
        days: true,
        hours: true,
        minutes: true,
        seconds: false,
    });

    $(document).on("click","#left ul.nav li.parent > div > span.sign", function () {
        $(this).find('i:first').toggleClass("fa-minus fa-plus");
    });

    // Open Le current menu
    $("#left ul.nav li.current").parents('ul.children').addClass("in");

    $('#add_attachment').click(function(){
        const max = 5
        if($('.file-container').length < max){
            axios('/api/get_file_input').then(result => {
                $('#attachment_container').append(result.data)
            })
        }else{
            toastr.warning("Maximum est de "+max);
        }
    })
    
    $(document).on('click', '.delete-file', function(){
        $(this).closest('.file-container').remove()
    })

    $('.delete-existing-file').click(function(){
        const filename = $(this).data('filename')
        const thisContainer = $(this).closest('.file-container')
        swal({
            title: "Attention!",
            text: "Êtes vous sûr de vouloir supprimer ce fichier?",
            icon: "warning",
            buttons: ["Non", "Oui"],
            dangerMode: true,
        }).then((confirmed) => {
            if (confirmed) {
                axios.post('/api/delete_newsletter_file', {
                    filename: filename,
                })
                .then(function (response) {
                    if(response.data.stat){
                        toastr.success(response.data.msg)
                        thisContainer.remove()
                    }else{
                        toastr.warning(response.data.msg)
                    }
                })
                .catch(function (error) {
                    toastr.warning('Oops! Une erreur est survenue!');
                });
            } else {
                return;
            }
        });
    })

})
