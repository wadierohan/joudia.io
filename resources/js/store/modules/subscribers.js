import { getSubscribers } from "@/api/subscribers";

// initial state
const state = {
    subscribers: []
}

// getters
const getters = {
    subscribers: state => { return state.subscribers }
}

// mutations
const mutations = {
    SET_SUBSCRIBERS: (state, subscribers) => {
        state.subscribers = subscribers
    }
}

// actions
const actions = {
    GetSubscribers: ( { commit }, payload ) => {
        return new Promise((resolve, reject) => {
            getSubscribers(payload.pagination, payload.filter)
                .then((result) => {
                    commit('SET_SUBSCRIBERS', result.data.data)
                    resolve(result.data)
                })
                .catch(error => {
                    console.error(error) // Just debugging..
                    reject(error)
                })
        })
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}