import { getForms } from "@/api/forms";

// initial state
const state = {
    forms: []
}

// getters
const getters = {
    forms: state => { return state.forms }
}

// mutations
const mutations = {
    SET_FORMS: (state, forms) => {
        state.forms = forms
    }
}

// actions
const actions = {
    GetForms: ({ commit }) => {
        return new Promise((resolve, reject) => {
            getForms()
                .then((result) => {
                    commit('SET_FORMS', result.data)
                    resolve(result.data)
                })
                .catch(error => {
                    console.error(error) // Just debugging..
                    reject(error)
                })
        })
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}