import { getFormations } from "@/api/formations";

// initial state
const state = {
    formations: []
}

// getters
const getters = {
    formations: state => { return state.formations }
}

// mutations
const mutations = {
    SET_FORMATIONS: (state, formations) => {
        state.formations = formations
    }
}

// actions
const actions = {
    GetFormations: ({ commit }) => {
        return new Promise((resolve, reject) => {
            getFormations()
                .then((result) => {
                    commit('SET_FORMATIONS', result.data)
                    resolve(result.data)
                })
                .catch(error => {
                    console.error(error) // Just debugging..
                    reject(error)
                })
        })
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}