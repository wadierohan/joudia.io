import { getTags } from "@/api/tags";

// initial state
const state = {
    tags: []
}

// getters
const getters = {
    tags: state => { return state.tags }
}

// mutations
const mutations = {
    SET_TAGS: (state, tags) => {
        state.tags = tags
    }
}

// actions
const actions = {
    GetTags: ({ commit }) => {
        return new Promise((resolve, reject) => {
            getTags()
                .then((result) => {
                    commit('SET_TAGS', result.data)
                    resolve(result.data)
                })
                .catch(error => {
                    console.error(error) // Just debugging..
                    reject(error)
                })
        })
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}