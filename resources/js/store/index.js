import Vue from 'vue'
import Vuex from 'vuex'
import subscribers from './modules/subscribers'
import tags from './modules/tags'
import forms from './modules/forms'
import formations from './modules/formations'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
    modules: {
        subscribers,
        tags,
        forms,
        formations
    },
    strict: debug
})