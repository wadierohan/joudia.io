import request from '@/utils/request'

export function getForms() {
    return request({
        url: '/api/forms',
        method: 'post'
    })
}