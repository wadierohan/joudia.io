import request from '@/utils/request'

export function getFormations() {
    return request({
        url: '/api/formations',
        method: 'post'
    })
}