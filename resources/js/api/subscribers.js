import request from '@/utils/request'

export function getSubscribers(pagination, filter) {
    const data = {
        pagination,
        filter
    }
    return request({
        url: '/api/subscribers/search',
        method: 'post',
        data,
        params: { page: pagination.page} 
    })
}