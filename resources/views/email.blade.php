@extends('layouts.email', ['title' => $subject])

@section('content')
    {!! $body !!}
    <img src="{!! route('queue.opened', ['queue' => $queue_id]) !!}" alt="" width="1" height="1" border="0" style="height:1px !important; width:1px !important; border-width:0 !important; margin-top:0 !important; margin-bottom:0 !important; margin-right:0 !important; margin-left:0 !important; padding-top:0 !important; padding-bottom:0 !important; padding-right:0 !important; padding-left:0 !important;" />
@endsection