@extends('layouts.app')

@section('content')
    @include('components.page-header', ['breadcrumb' => [__('Expéditeur'), __('Nouveau')], 'new_btn_link' => ''])

    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="card shadow">
                <div class="card-body">
                    {!! Form::open([ 'method'  => 'POST', 'route' => [ 'senders.store' ], 'autocomplete' => 'off' ]) !!}
                        <div class="form-group row">
                            {!! Form::label('name', __('Nom'), ['class' => 'col-sm-4 col-form-label text-md-right']) !!}
                            <div class="col-sm-6">
                                {!! Form::text('name', null, ['class' => 'form-control'.($errors->has('name') ? ' is-invalid' : ''), 'required' => 'required']) !!}
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $errors->first('name') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('from_name', __('Nom de l\'expéditeur'), ['class' => 'col-sm-4 col-form-label text-md-right']) !!}
                            <div class="col-sm-6">
                                {!! Form::text('from_name', null, ['class' => 'form-control'.($errors->has('from_name') ? ' is-invalid' : ''), 'required' => 'required']) !!}
                                @if ($errors->has('from_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $errors->first('from_name') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('from_email', __('Email de l\'expéditeur'), ['class' => 'col-sm-4 col-form-label text-md-right']) !!}
                            <div class="col-sm-6">
                                {!! Form::email('from_email', null, ['class' => 'form-control'.($errors->has('from_email') ? ' is-invalid' : ''), 'required' => 'required']) !!}
                                @if ($errors->has('from_email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $errors->first('from_email') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('reply_name', __('Nom de l\'email de réponse'), ['class' => 'col-sm-4 col-form-label text-md-right']) !!}
                            <div class="col-sm-6">
                                {!! Form::text('reply_name', null, ['class' => 'form-control'.($errors->has('reply_name') ? ' is-invalid' : ''), 'required' => 'required']) !!}
                                @if ($errors->has('reply_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $errors->first('reply_name') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('reply_email', __('Email de réponse'), ['class' => 'col-sm-4 col-form-label text-md-right']) !!}
                            <div class="col-sm-6">
                                {!! Form::email('reply_email', null, ['class' => 'form-control'.($errors->has('reply_email') ? ' is-invalid' : ''), 'required' => 'required']) !!}
                                @if ($errors->has('reply_email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $errors->first('reply_email') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('mailserver_id', __('Serveur mail'), ['class' => 'col-sm-4 col-form-label text-md-right']) !!}
                            <div class="col-sm-6">
                                {!! Form::select('mailserver_id', Auth::user()->mailservers()->pluck('mailservers.name', 'mailservers.id'), null, ['class' => 'form-control'.($errors->has('mailserver_id') ? ' is-invalid' : '')]) !!}
                                @if ($errors->has('mailserver_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $errors->first('mailserver_id') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-sm-4 col-sm-6 text-right">
                                {!! Form::submit(__('Valider'), ['class' => 'btn btn-primary']) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection