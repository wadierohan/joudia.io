@extends('layouts.app')

@section('content')
@include('components.page-header', ['breadcrumb' => [__('Expéditeurs')], 'new_btn_link' => route('senders.create')])
<div class="row">
    @foreach(Auth::User()->senders as $row)
    <div class="col-md-4">
        <div class="card bg-dark text-white trigger-options shadow">
            <div class="card-body">
                <h4 class="card-title">{!! $row->name !!}</h4>
                <p class="card-text">{!! __('Expéditeur') !!} : {!! $row->from_name !!} - {!! $row->from_email !!}</p>
                <p class="card-text">{!! __('Répondre à') !!} : {!! $row->reply_name !!} - {!! $row->reply_email !!}</p>
                <div class="options-hover float-right">
                    <a href="{!!route('senders.edit', $row->id)!!}"><i class="fas fa-edit"></i></a>
                    <i class="fas fa-trash-alt delete-submit-form"></i>
                    {!! Form::open([ 'method'  => 'delete', 'route' => [ 'senders.destroy', $row->id ] ]) !!}{!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endsection