@extends('layouts.app')

@section('content')
    {!! Form::open([ 'method'  => 'patch', 'route' => [ 'forms.update', $form->id ], 'autocomplete' => 'off' ]) !!}
        @include('components.page-header', ['breadcrumb' => [__('Formulaire'), $form->name], 'new_btn_link' => '', 'submit_btn' => true])
        <div class="row">
            <div class="col-lg-8">
                <div class="card mb-3 shadow">
                    <div class="card-header">
                        {!! __('Email de bienvenue') !!}
                    </div>
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                {!! Form::label('sender_id', __('Expéditeur'), ['class' => 'col-form-label']) !!}
                                {!! Form::select('sender_id', Auth::user()->senders()->pluck('senders.name', 'senders.id'), $form->sender_id, ['class' => 'form-control'.($errors->has('sender_id') ? ' is-invalid' : '')]) !!}
                                @if ($errors->has('sender_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $errors->first('sender_id') !!}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                {!! Form::label('human_delay', __('Délai'), ['class' => 'col-form-label']) !!}
                                {!! Form::hidden('delay', $form->delay, ['class' => 'delay']) !!}
                                {!! Form::text('human_delay', null, ['class' => 'durationpicker form-control'.($errors->has('delay') ? ' is-invalid' : ''), 'placeholder' => __('Immediatement')]) !!}
                                @if ($errors->has('delay'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $errors->first('delay') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                {!! Form::label('subject', __('Subject'), ['class' => 'col-form-label']) !!}
                                {!! Form::text('subject', $form->newsletter ? $form->newsletter->subject : null, ['class' => 'form-control'.($errors->has('subject') ? ' is-invalid' : '')]) !!}
                                @if ($errors->has('subject'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $errors->first('subject') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                {!! Form::label('body', __('Corps'), ['class' => 'col-form-label']) !!}
                                {!! Form::textarea('body', $form->newsletter ? $form->newsletter->body : null, ['class' => 'form-control'.($errors->has('body') ? ' is-invalid' : '')]) !!}
                                @if ($errors->has('body'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $errors->first('body') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card mb-3 shadow">
                    <div class="card-header">
                        {!! __('Informations') !!}
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            {!! Form::label('name', __('Nom'), ['class' => 'col-form-label']) !!}
                            {!! Form::text('name', $form->name, ['class' => 'form-control'.($errors->has('name') ? ' is-invalid' : ''), 'required' => 'required']) !!}
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('name') !!}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            {!! Form::label('type', __('Type'), ['class' => 'col-form-label']) !!}
                            {!! Form::select('type', ['form' => __('Formulaire'), 'popup' => __('Popup')], $form->type, ['class' => 'form-control'.($errors->has('type') ? ' is-invalid' : ''), 'required' => 'required']) !!}
                            @if ($errors->has('type'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('type') !!}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            {!! Form::label('tags', __('Tags'), ['class' => 'col-form-label']) !!}
                            {!! Form::select('tags', Auth::user()->tags()->pluck('tags.name', 'tags.id'), $form->tags, ['class' => 'form-control'.($errors->has('tags') ? ' is-invalid' : ''), 'required' => 'required']) !!}
                            @if ($errors->has('tags'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('tags') !!}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            {!! Form::label('redirect', __('Lien de redirection'), ['class' => 'col-form-label']) !!}
                            {!! Form::text('redirect', $form->redirect, ['class' => 'form-control'.($errors->has('redirect') ? ' is-invalid' : '')]) !!}
                            @if ($errors->has('redirect'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('redirect') !!}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            {!! Form::label('success_msg', __('Message d\'inscription avec succès'), ['class' => 'col-form-label']) !!}
                            {!! Form::textarea('success_msg', $form->success_msg, ['class' => 'form-control'.($errors->has('success_msg') ? ' is-invalid' : '')]) !!}
                            @if ($errors->has('success_msg'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('success_msg') !!}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>   
    {!! Form::close() !!}
@endsection