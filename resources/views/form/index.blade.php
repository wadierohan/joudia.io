@extends('layouts.app')

@section('content')
    @include('components.page-header', ['breadcrumb' => [__('Formulaires')], 'new_btn_link' => route('forms.create')])

    <div class="table-responsive shadow">
        <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                    <th>{!! __('Nom') !!}</th>
                    <th>{!! __('Type') !!}</th>
                    <th>{!! __('Code') !!}</th>
                    <th>{!! __('Visiteurs') !!}</th>
                    <th>{!! __('Inscrits') !!}</th>
                    <th>{!! __('Taux de conversion') !!}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach (Auth::user()->forms as $row)
                <tr class="trigger-options">
                    <td>{!! $row->name !!}</td>
                    <td>{!! $row->type !!}</td>
                    <td>
                        <button class="btn copy-clipboard" data-clipboard-text='<script data-uniqid="{!! $row->uniq_id !!}" type="text/javascript" src="{!! asset('js/getform.js') !!}"></script>' type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="{!! __('Copié') !!}">
                            {!! __('Copier code') !!}
                        </button>
                    </td>
                    <td>{!! $row->visitors !!}</td>
                    <td>{!! $row->subscribers()->count() !!}</td>
                    <td>
                        {!! ($row->visitors > 0 ? ($row->subscribers()->count() / $row->visitors) * 100 : 0) . ' %' !!}
                    </td>
                    <td>
                        <div class="options-hover float-right">
                            <a title="Editeur" href="{!!route('forms.wysiwyg', $row->id)!!}"><i class="fas fa-file-alt text-dark"></i></a>
                            <a title="Paramètres" href="{!!route('forms.edit', $row->id)!!}"><i class="fas fa-edit text-dark"></i></a>
                            <i title="Supprimer" class="fas fa-trash-alt delete-submit-form text-dark"></i>
                            {!! Form::open([ 'method'  => 'delete', 'route' => [ 'forms.destroy', $row->id ] ]) !!}{!! Form::close() !!}
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@endsection
