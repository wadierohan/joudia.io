<!DOCTYPE html>
<html lang="{!! str_replace('_', '-', app()->getLocale()) !!}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{----------------------------- CSRF Token ------------------------------}}
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <meta name="base-url" content="{{ url('/') }}" />

    <title>{!! config('app.name', 'Laravel') !!}</title>

    {{----------------------------- Script ------------------------------}}
    <script src="/js/app.js"></script>
    @include('components.flash')
    @include('components.tinymce')
    @include('components.select2')
    @include('components.grapesjs')

    {{----------------------------- Styles ------------------------------}}
    <link href="/css/app.css" rel="stylesheet">
</head>
<body>
    <main id="app">
        @yield('content')
    </main>
    @yield('script')
</body>
</html>
