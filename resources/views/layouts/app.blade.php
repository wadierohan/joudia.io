<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{----------------------------- CSRF Token ------------------------------}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="base-url" content="{{ url('/') }}" />

    <title>{{ config('app.name', 'Joudia.io') }}</title>

    {{----------------------------- Styles ------------------------------}}
    <link href="/css/app.css" rel="stylesheet">
</head>
<body>
    @auth
        <nav id="first-menu" class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="{!! url('/') !!}">
                {!! config('app.name', 'Joudia.io') !!}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsemenu" aria-controls="collapsemenu" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            {{----------------------------- Right Side Of Navbar ------------------------------}}
            <ul class="navbar-nav ml-auto">
                {{----------------------------- Authentication Links ------------------------------}}
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {!! Auth::user()->name !!} <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{!! route('logout') !!}"
                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                            {!! __('Déconnexion') !!}
                        </a>

                        <form id="logout-form" action="{!! route('logout') !!}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            </ul>
        </nav>

        <nav id="second-menu" class="navbar sticky-top navbar-expand-lg navbar-light bg-light shadow">
            {{----------------------------- Left Side Of Navbar ------------------------------}}
            <div id="collapsemenu" class="collapse navbar-collapse">
                <ul class="navbar-nav mr-auto">
                    @auth('web')
                        <li class="nav-item {!! strpos(Route::currentRouteName(), 'dashboard') !== false  ? 'active' : ''!!}">
                            <a class="nav-link scale" href="{!! route('dashboard') !!}"><i class="fas fa-tachometer-alt"></i> {!! __('Tableau de bord') !!}</a>
                        </li>
                        <li class="nav-item {!! strpos(Route::currentRouteName(), 'mailservers') !== false ? 'active' : ''!!}">
                            <a class="nav-link scale" href="{!! route('mailservers.index') !!}"><i class="fas fa-server"></i> {!! __('Serveurs mail') !!}</a>
                        </li>
                        <li class="nav-item {!! strpos(Route::currentRouteName(), 'senders') !== false ? 'active' : ''!!}">
                            <a class="nav-link scale" href="{!! route('senders.index') !!}"><i class="fas fa-address-card"></i> {!! __('Expéditeurs') !!}</a>
                        </li>
                        <li class="nav-item {!! strpos(Route::currentRouteName(), 'newsletters') !== false ? 'active' : ''!!}">
                            <a class="nav-link scale" href="{!! route('newsletters.index') !!}"><i class="fas fa-envelope"></i> {!! __('Newsletters') !!}</a>
                        </li>
                        <li class="nav-item {!! strpos(Route::currentRouteName(), 'fields') !== false ? 'active' : ''!!}">
                            <a class="nav-link scale" href="{!! route('fields.index') !!}"><i class="far fa-address-card"></i> {!! __('Champs') !!}</a>
                        </li>
                        <li class="nav-item {!! strpos(Route::currentRouteName(), 'tags') !== false ? 'active' : ''!!}">
                            <a class="nav-link scale" href="{!! route('tags.index') !!}"><i class="fas fa-tags"></i> {!! __('Tags') !!}</a>
                        </li>
                        <li class="nav-item {!! strpos(Route::currentRouteName(), 'subscribers') !== false ? 'active' : ''!!}">
                            <a class="nav-link scale" href="{!! route('subscribers.index') !!}"><i class="fas fa-users"></i> {!! __('Abonnés') !!}</a>
                        </li>
                        <li class="nav-item {!! strpos(Route::currentRouteName(), 'forms') !== false ? 'active' : ''!!}">
                            <a class="nav-link scale" href="{!! route('forms.index') !!}"><i class="fas fa-file-alt"></i> {!! __('Formulaires') !!}</a>
                        </li>
                        <li class="nav-item {!! strpos(Route::currentRouteName(), 'schedules') !== false ? 'active' : ''!!}">
                            <a class="nav-link scale" href="{!! route('schedules.index') !!}"><i class="fas fa-clock"></i> {!! __('Plannifications') !!}</a>
                        </li>
                        <li class="nav-item {!! strpos(Route::currentRouteName(), 'queues') !== false ? 'active' : ''!!}">
                            <a class="nav-link scale" href="{!! route('queues.index') !!}"><i class="fas fa-layer-group"></i> {!! __('Queues') !!}</a>
                        </li>
                        <li class="nav-item {!! strpos(Route::currentRouteName(), 'campaigns') !== false ? 'active' : ''!!}">
                            <a class="nav-link scale" href="{!! route('campaigns.index') !!}"><i class="fas fa-bullhorn"></i> {!! __('Campagnes') !!}</a>
                        </li>
                        <li class="nav-item {!! strpos(Route::currentRouteName(), 'formations') !== false ||
                                                strpos(Route::currentRouteName(), 'modules') !== false ||
                                                strpos(Route::currentRouteName(), 'chapters') !== false ? 'active' : ''!!}">
                            <a class="nav-link scale" href="{!! route('formations.index') !!}"><i class="fas fa-chalkboard-teacher"></i> {!! __('Formations') !!}</a>
                        </li>
                    @endauth
                </ul>
            </div>
        </nav>
    @endauth
    <div class="container-fluid">
        <main id="app">
            @yield('content')
        </main>
    </div>
    
    {{----------------------------- Script ------------------------------}}
    <script src="/js/app.js"></script>
    @include('components.flash')
    @include('components.tinymce')
    @include('components.select2')
    @yield('script')
</body>
</html>
