<!DOCTYPE html>
<html lang="{!! str_replace('_', '-', app()->getLocale()) !!}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{----------------------------- CSRF Token ------------------------------}}
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <meta name="base-url" content="{{ url('/') }}" />

    <title>{!! config('app.name', 'Laravel') !!}</title>

    {{----------------------------- Script ------------------------------}}
    <script src="/js/app.js"></script>
    @include('components.flash')
    @include('components.tinymce')
    @include('components.select2')

    {{----------------------------- Styles ------------------------------}}
    <link href="/css/app.css" rel="stylesheet">
</head>
<body>
    <div class="container-fluid">
        <header>
            <div class="row">
                <div id="top-menu">
                    <nav id="primary-menu" class="navbar navbar-expand-lg navbar-light bg-dark">
                        <a class="navbar-brand" href="{!! url('/') !!}">
                            {!! config('app.name', 'Joudia.io') !!}
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsemenu" aria-controls="collapsemenu" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        {{----------------------------- Left Side Of Navbar ------------------------------}}
                        <div id="collapsemenu" class="collapse navbar-collapse">
                            <ul class="navbar-nav mr-auto">
                                
                            </ul>
                        </div>

                        {{----------------------------- Right Side Of Navbar ------------------------------}}
                        <ul class="navbar-nav ml-auto">
                            {{----------------------------- Authentication Links ------------------------------}}
                            @auth
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {!! Auth::user()->email !!} <span class="caret"></span>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{!! route('logout') !!}"
                                            onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();">
                                            {!! __('Déconnexion') !!}
                                        </a>

                                        <form id="logout-form" action="{!! route('logout') !!}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            @endauth
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
        <main id="app">
            @yield('content')
        </main>
    </div>
    @yield('script')
</body>
</html>
