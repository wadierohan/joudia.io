<div class="page-header mb-3">
    <h3 class="float-left">
        {!! implode(' <i class="fas fa-chevron-right"></i> ', $breadcrumb) !!}
        
    </h3> 
    <div class="float-right">
        @if(isset($new_btn_link) && $new_btn_link)
        <a href="{{ $new_btn_link }}" class="btn btn-primary">@lang('Nouveau')</a>
        @endif

        <div class="btn-group" role="group">
            @if(isset($submit_btn) && $submit_btn)
            <button title="Enregistrer" type="submit" name="submit" value="save" class="btn btn-primary"><i class="fas fa-save"></i></button>
            <button title="Enregistrer et nouveau" type="submit" name="submit" value="savenew" class="btn btn-primary"><i class="fas fa-share-square"></i></button>
            @endif

            @if(isset($exitUrl) && $exitUrl)
            <a href="{{ $exitUrl }}" class="btn btn-primary"><i class="fas fa-sign-out-alt"></i></a>
            @endif
        </div>
    </div>
    <div class="clearfix"></div>  
</div>