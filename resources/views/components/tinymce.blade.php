<script>
    $(function(){
        const custom_fields = {!! Auth::check() && Auth::user()->fields ? Auth::user()->fields->pluck('uniq_id', 'name')->toJson() : '{}' !!};
        const tinymce_tags = Object.assign({'Email' : 'email'}, custom_fields);
        tinymce.init({ 
            selector:'textarea',
            height: 200,
            plugins: 'print preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help tag',
            toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | tag',
            image_advtab: true,
            contextmenu: "tagDropDownMenu link image inserttable | cell row column deletetable",
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ],
            tags: tinymce_tags,
        });
    })
</script>