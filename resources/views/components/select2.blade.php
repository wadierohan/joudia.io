<script>
    $(function(){
        $('select.tag').select2({
            theme: "bootstrap",
            closeOnSelect: false,
            width: 'style'
        });

        $('select.multi-tag').select2({
            theme: "bootstrap",
            closeOnSelect: false,
            tags: true,
            width: 'style',
            createTag: function (tag) {
                return {
                    id: tag.term,
                    text: tag.term,
                    isNew : true
                };
            }
        }).on("select2:select", function(e) {
            if(e.params.data.isNew){
                var name = e.params.data.text;
                var that = $(this);
                that.find('option[data-select2-tag="true"]').remove();
                $.ajax({
                    url: "{{ route('tags.apistore') }}",
                    type: 'POST',
                    dataType: "json",
                    data: {'name':name},
                    success: function(response){
                        if(response.success == 1){
                            var option = new Option(response.name+' (0)', response.id, true, true);
                            that.append(option).trigger('change');
                        }
                    },
                    error: function(data){
                        console.log(data);
                    },
                });
            }
        });
    })
</script>