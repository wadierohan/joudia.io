<script>
    $(function(){
      var editor = grapesjs.init({
          protectedCss: '',
          container : '#wysiwyg',
          showOffsets : true,
          height: '100vh',
          plugins: [
              'gjs-preset-joudia',
          ],
          pluginsOpts: {
              'gjs-preset-joudia': {
                  'csrf': '{{ csrf_token() }}',
                  'submitUrl': joudiaJS.submitUrl,
                  'exitUrl': joudiaJS.exitUrl,
                  'formsOpts': {
                      'joudiaTags': joudiaJS.tags,
                      'joudiaFields': joudiaJS.fields,
                      'joudiaFormSubmitUrl': '{{route('forms.submitForm')}}'
                  }
              }
          },
          fromElement: true,

          storageManager: {
              type: 'remote',
              autoload: false,
              autosave: false,
              stepsBeforeSave: 3,
              contentTypeJson: true,
              urlStore: '{{route('storegjs')}}',
              urlLoad: '{{route('loadgjs', Auth::id())}}',
              params: {'userid' : {{Auth::id()}}},
          },

          assetManager: {
              // Upload endpoint, set `false` to disable upload, default `false`
              upload: '{{route('uploaduserimage')}}',
              params: {'userid':{{Auth::id()}}},

              // The name used in POST to pass uploaded files, default: `'files'`
              uploadName: 'files',
              autoAdd: true,
          },
      });
    })
</script>
