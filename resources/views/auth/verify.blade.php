@extends('layouts.app')

@section('content')
    <div id="form-container" class="centred">
        <div class="card">
            <div class="card-header bg-dark text-light text-center">{!! __('Vérification de votre adresse email') !!}</div>

            <div class="card-body">
                @if (session('resent'))
                    <div class="alert alert-success" role="alert">
                        {!! __('Un nouveau lien de vérification a été envoyé à votre adresse email.') !!}
                    </div>
                @endif

                {!! __('Avant de procéder, veuillez vérifier votre adresse email pour le lien de vérification.') !!}
                {!! __('Si vous n\'avez pas reçu l\'email') !!}, <a href="{!! route('verification.resend') !!}">{!! __('Cliquez ici pour avoir un autre') !!}</a>.
            </div>
        </div>
    </div>
@endsection
