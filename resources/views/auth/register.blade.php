@extends('layouts.app')

@section('content')
    <div id="form-container" class="centred">
        <div id="register-form" class="card">
            <div class="card-header bg-dark text-light text-center">
                <h1>@lang('Inscription')</h1>
            </div>
            <div class="card-body">
                <form method="POST" action="{!! route('register') !!}">
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label class="sr-only" for="domain">@lang('Domaine')</label>
                            <div class="input-group">
                                <input placeholder="@lang('Domaine')" aria-describedby="domain-addon" id="domain" type="text" class="form-control {!! $errors->has('domain') ? 'is-invalid' : '' !!}" name="domain" value="{!! old('domain') !!}" required autofocus>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="domain-addon"><i class="fas fa-link"></i></span>
                                </div>
                                @if ($errors->has('domain'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $errors->first('domain') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label class="sr-only" for="name">@lang('Nom')</label>
                            <div class="input-group">
                                <input placeholder="@lang('Nom')" aria-describedby="name-addon" id="name" type="text" class="form-control {!! $errors->has('name') ? 'is-invalid' : '' !!}" name="name" value="{!! old('name') !!}" required>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="name-addon"><i class="fas fa-signature"></i></span>
                                </div>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $errors->first('name') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label class="sr-only" for="email">@lang('Email')</label>
                            <div class="input-group">
                                <input placeholder="@lang('Email')" aria-describedby="email-addon" id="email" type="email" class="form-control {!! $errors->has('email') ? 'is-invalid' : '' !!}" name="email" value="{!! old('email') !!}" required>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="email-addon"><i class="fas fa-at"></i></span>
                                </div>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $errors->first('email') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label class="sr-only" for="password">@lang('Mot de passe')</label>
                            <div class="input-group">
                                <input placeholder="@lang('Mot de passe')" aria-describedby="password-addon" id="password" type="password" class="form-control {!! $errors->has('password') ? 'is-invalid' : '' !!}" name="password" value="" required>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="password-addon"><i class="fas fa-lock"></i></span>
                                </div>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $errors->first('password') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label class="sr-only" for="password_confirmation">@lang('Confirmation du mot de passe')</label>
                            <div class="input-group">
                                <input placeholder="@lang('Confirmation du mot de passe')" aria-describedby="password_confirmation-addon" id="password_confirmation" type="password" class="form-control {!! $errors->has('password_confirmation') ? 'is-invalid' : '' !!}" name="password_confirmation" value="" required>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="password_confirmation-addon"><i class="fas fa-lock"></i></span>
                                </div>
                                @if ($errors->has('password_confirmation'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $errors->first('password_confirmation') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary btn-block">
                                {!! __('Inscription') !!}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="card-footer text-center">
                <a class="btn btn-link" href="{!! route('login') !!}">
                    <h4>@lang("Connexion")</h4>
                </a>
            </div>
        </div>
    </div>
@endsection
