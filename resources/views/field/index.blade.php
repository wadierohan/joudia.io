@extends('layouts.app')

@section('content')
    @include('components.page-header', ['breadcrumb' => [__('Champs')], 'new_btn_link' => route('fields.create')])

    <div class="row">
        <div class="col-md-4 offset-md-4">
            <ul class="list-group">
                <li class="list-group-item bg-secondary text-light">
                    <span>Email</span>
                </li>
                @foreach (Auth::user()->fields as $row)
                <li class="list-group-item trigger-options bg-dark text-light">
                    <span>{{ $row->name }}</span>
                    <div class="options-hover float-right">
                        <a href="{!!route('fields.edit', $row->id)!!}"><i class="fas fa-edit "></i></a>
                        <i class="fas fa-trash-alt delete-submit-form "></i>
                        {!! Form::open([ 'method'  => 'delete', 'route' => [ 'fields.destroy', $row->id ] ]) !!}{!! Form::close() !!}
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </div>

@endsection