@extends('layouts.app')

@section('content')
    @include('components.page-header', ['breadcrumb' => [__('Champ'), $field->name], 'new_btn_link' => ''])
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="card shadow">
                <div class="card-body">
                    {!! Form::open([ 'method'  => 'patch', 'route' => [ 'fields.update', $field->id ], 'autocomplete' => 'off' ]) !!}
                        <div class="form-group row">
                            {!! Form::label('name', __('Nom'), ['class' => 'col-sm-4 col-form-label text-md-right']) !!}
                            <div class="col-sm-6">
                                {!! Form::text('name', $field->name, ['class' => 'form-control'.($errors->has('name') ? ' is-invalid' : ''), 'required' => 'required']) !!}
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $errors->first('name') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-sm-4 col-sm-6 text-right">
                                {!! Form::submit(__('Valider'), ['class' => 'btn btn-primary']) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection