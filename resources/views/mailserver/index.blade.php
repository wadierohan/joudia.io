@extends('layouts.app')

@section('content')
    @include('components.page-header', ['breadcrumb' => [__('Serveur mail')], 'new_btn_link' => route('mailservers.create')])
    
    <div class="row">
        @foreach(Auth::User()->mailservers as $row)
        <div class="col-md-4 mb-3">
            <div class="card bg-dark text-white trigger-options shadow">
                <div class="card-body">
                    <h3>{!! $row->name !!}</h3>
                    <div class="options-hover float-right">
                        <a href="{!!route('mailservers.edit', $row->id)!!}"><i class="fas fa-edit"></i></a>
                        <i class="fas fa-trash-alt delete-submit-form"></i>
                        <form method="post" action="{{ route('mailservers.destroy', $row->id) }}">@csrf @method('delete')</form>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
@endsection