@extends('layouts.app')

@section('content')
    @include('components.page-header', ['breadcrumb' => [__('Serveur mail'), $mailserver->name], 'new_btn_link' => ''])

    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="card shadow">
                <div class="card-body">
                    {!! Form::open([ 'method'  => 'patch', 'route' => [ 'mailservers.update', $mailserver->id ], 'autocomplete' => 'off' ]) !!}
                        <div class="form-group row">
                            {!! Form::label('name', __('Nom'), ['class' => 'col-sm-4 col-form-label text-md-right']) !!}
                            <div class="col-sm-6">
                                {!! Form::text('name', $mailserver->name, ['class' => 'form-control'.($errors->has('name') ? ' is-invalid' : ''), 'required' => 'required']) !!}
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $errors->first('name') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('host', __('Host'), ['class' => 'col-sm-4 col-form-label text-md-right']) !!}
                            <div class="col-sm-6">
                                {!! Form::text('host', $mailserver->host, ['class' => 'form-control'.($errors->has('host') ? ' is-invalid' : ''), 'required' => 'required']) !!}
                                @if ($errors->has('host'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $errors->first('host') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('port', __('Port'), ['class' => 'col-sm-4 col-form-label text-md-right']) !!}
                            <div class="col-sm-6">
                                {!! Form::number('port', $mailserver->port, ['class' => 'form-control'.($errors->has('port') ? ' is-invalid' : ''), 'min' => 0, 'required' => 'required']) !!}
                                @if ($errors->has('port'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $errors->first('port') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('encryption', __('Encryption'), ['class' => 'col-sm-4 col-form-label text-md-right']) !!}
                            <div class="col-sm-6">
                                {!! Form::select('encryption', ['' => __('None'), 'ssl' => 'SSL', 'tls' => 'STARTTLS'], $mailserver->encryption, ['class' => 'form-control'.($errors->has('encryption') ? ' is-invalid' : '')]) !!}
                                @if ($errors->has('encryption'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $errors->first('encryption') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('username', __('Nom d\'utilisateur'), ['class' => 'col-sm-4 col-form-label text-md-right']) !!}
                            <div class="col-sm-6">
                                {!! Form::text('username', $mailserver->username, ['class' => 'form-control', 'autocomplete' => 'off']) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('password', __('Mot de passe'), ['class' => 'col-sm-4 col-form-label text-md-right']) !!}
                            <div class="col-sm-6">
                                {!! Form::password('password', ['class' => 'form-control', 'autocomplete' => 'new-password']) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-sm-4 col-sm-6 text-right">
                                {!! Form::submit(__('Valider'), ['class' => 'btn btn-primary']) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection