@extends('layouts.app')

@section('content')
    @include('components.page-header', ['breadcrumb' => [__('Newsletters')], 'new_btn_link' => route('newsletters.create')])

    <div class="row">
        @foreach(Auth::User()->newsletters->where('type', 'news') as $row)
        <div class="col-md-4">
            <div class="card bg-dark text-white trigger-options shadow">
                <div class="card-body">
                    <h4 class="card-title">{!! $row->name !!}</h4>
                    <div class="options-hover float-right">
                        <a data-toggle="tooltip" data-placement="top" title="{!! __('Queue') !!}" href="{!!route('queues.newsletter', $row->id)!!}"><i class="fas fa-layer-group"></i></a>
                        <a href="{!!route('newsletters.edit', $row->id)!!}"><i class="fas fa-edit"></i></a>
                        <i class="fas fa-trash-alt delete-submit-form"></i>
                        {!! Form::open([ 'method'  => 'delete', 'route' => [ 'newsletters.destroy', $row->id ] ]) !!}{!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    
@endsection