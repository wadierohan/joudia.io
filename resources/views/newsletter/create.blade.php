@extends('layouts.app')

@section('content')
    {!! Form::open([ 'method'  => 'POST', 'route' => [ 'newsletters.store' ], 'autocomplete' => 'off', 'files' => true ]) !!}
    @include('components.page-header', ['breadcrumb' => [__('Newsletter'), __('Nouveau')], 'new_btn_link' => '', 'submit_btn' => true])

    <div class="row">
        <div class="col-md-8">
            <div class="card shadow">
                <div class="card-body">
                    <div class="form-group">
                        {!! Form::label('subject', __('Sujet'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::text('subject', null, ['class' => 'form-control'.($errors->has('subject') ? ' is-invalid' : ''), 'required' => 'required']) !!}
                        @if ($errors->has('subject'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('subject') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('body', __('Corps'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::textarea('body', null, ['class' => 'form-control'.($errors->has('body') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('body'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('body') !!}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card shadow mb-3">
                <div class="card-header">
                    @lang('Paramètres')
                </div>
                <div class="card-body">
                    <div class="form-group">
                        {!! Form::label('name', __('Nom'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::text('name', null, ['class' => 'form-control'.($errors->has('name') ? ' is-invalid' : ''), 'required' => 'required']) !!}
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('name') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('sender_id', __('Expéditeur'), ['class' => 'col-form-label']) !!}
                        {!! Form::select('sender_id', Auth::user()->senders()->pluck('senders.name', 'senders.id'), null, ['class' => 'form-control'.($errors->has('sender_id') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('sender_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('sender_id') !!}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="card shadow">
                <div class="card-header">
                    @lang('Pièces jointes')
                </div>
                <div class="card-body">
                    <button type="button" id="add_attachment" class="btn btn-primary btn-lg btn-block">@lang('Ajouter une pièce jointe')</button>
                    <div id="attachment_container">
                        
                    </div>
                    @if($errors->has('attachments.*'))
                        <div class="alert alert-danger" role="alert">
                            <ul>
                            @foreach($errors->get('attachments.*') as $msg)
                                <li>{{ head($msg) }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection