@extends('layouts.app')

@section('content')
    @include('components.page-header', ['breadcrumb' => [__('Abonnés')], 'new_btn_link' => route('subscribers.create')])

    <table-subscribers />
@endsection