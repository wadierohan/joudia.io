@extends('layouts.app')

@section('content')
    {!! Form::open([ 'method'  => 'patch', 'route' => [ 'subscribers.update', $subscriber->id ], 'autocomplete' => 'off' ]) !!}
    @include('components.page-header', ['breadcrumb' => [__('Abonné'), $subscriber->email], 'submit_btn' => true])
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card mb-3 shadow">
                    <div class="card-body">
                        <div class="form-group">
                            {!! Form::label('email', __('Email'), ['class' => '']) !!}
                            {!! Form::text(null, $subscriber->email, ['class' => 'form-control', 'disabled' => 'disabled', 'id' => 'email']) !!}
                        </div>
                        @foreach($fields as $field)
                        <div class="form-group">
                            {!! Form::label($field->uniq_id, $field->name, ['class' => '']) !!}
                            {!! Form::text($field->uniq_id, $subscriber->fieldValue($field->id), ['class' => 'form-control', 'id' => $field->uniq_id]) !!}
                        </div>
                        @endforeach
                        <hr>
                        <div class="form-group">
                            {!! Form::label('tags', __('Tags'), ['class' => '']) !!}
                            {!! Form::select('tags[]', $tags->pluck('name', 'id'), $subscriber->tags->pluck('id'), ['class' => 'form-control multi-tag'.($errors->has('tags') ? ' is-invalid' : ''), 'multiple' => 'multiple']) !!}
                            @if ($errors->has('tags'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('tags') !!}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            {!! Form::label('formations', __('Formations'), ['class' => '']) !!}
                            {!! Form::select('formations[]', $formations->pluck('name', 'id'), $subscriber->formations->pluck('id'), ['class' => 'form-control tag'.($errors->has('formations') ? ' is-invalid' : ''), 'multiple' => 'multiple']) !!}
                            @if ($errors->has('formations'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('formations') !!}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card mb-3 shadow">
                    <div class="card-header">
                        <h3>@lang('Historique des tags')</h3>
                    </div>
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>@lang('Date')</th>
                                        <th>@lang('Tag')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($subscriber->tags as $tag)
                                        <tr>
                                            <td>{{ $tag->pivot->created_at }}</td>
                                            <td>{{ $tag->name }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card mb-3 shadow">
                    <div class="card-header">
                        <h3>@lang('Formations')</h3>
                    </div>
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>@lang('Date')</th>
                                        <th>@lang('Formation')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($subscriber->formations as $formation)
                                        <tr>
                                            <td>{{ $formation->pivot->created_at }}</td>
                                            <td>{{ $formation->name }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="card mb-3 shadow">
                    <div class="card-header">
                        <h3>@lang('Queues')</h3>
                    </div>
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>{!! __('Newsletter') !!}</th>
                                        <th>{!! __('Essai d\'envoi échoué') !!}</th>
                                        <th>{!! __('Date d\'envoi') !!}</th>
                                        <th>{!! __('Envoyé') !!}</th>
                                        <th>{!! __('Ouvert') !!}</th>
                                        <th>{!! __('Date d\'ouverture') !!}</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($subscriber->queues as $queue)
                                        <tr class="trigger-options">
                                            <td>{{ $queue->newsletter->name }}</td>
                                            <td>{{ $queue->try }}</td>
                                            <td>{{ $queue->send_at }}</td>
                                            <td>{{ $queue->sent ? __('Oui') : __('Non') }}</td>
                                            <td>{{ $queue->opened ? __('Oui') : __('Non') }}</td>
                                            <td>{{ $queue->opened_at }}</td>
                                            <td>
                                                <div class="options-hover float-right">
                                                    <i class="fas fa-trash-alt delete-submit-form text-dark"></i>
                                                    {!! Form::open([ 'method'  => 'delete', 'route' => [ 'queues.destroy', $queue->id ] ]) !!}{!! Form::close() !!}
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    {!! Form::close() !!}
@endsection