@extends('layouts.app')

@section('content')
    {!! Form::open([ 'method'  => 'post', 'route' => [ 'subscribers.store' ], 'autocomplete' => 'off' ]) !!}
    @include('components.page-header', ['breadcrumb' => [__('Abonné'), __('Nouveau')]])
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card mb-3 shadow">
                    <div class="card-body">
                        <div class="form-group">
                            {!! Form::label('email', __('Email'), ['class' => '']) !!}
                            {!! Form::text('email', null, ['class' => 'form-control'.($errors->has('email') ? ' is-invalid' : ''), 'id' => 'email']) !!}
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('email') !!}</strong>
                                </span>
                            @endif
                        </div>
                        @foreach($fields as $field)
                        <div class="form-group">
                            {!! Form::label($field->uniq_id, $field->name, ['class' => '']) !!}
                            {!! Form::text($field->uniq_id, null, ['class' => 'form-control', 'id' => $field->uniq_id]) !!}
                        </div>
                        @endforeach
                        <hr>
                        <div class="form-group">
                            {!! Form::label('tags', __('Tags'), ['class' => '']) !!}
                            {!! Form::select('tags[]', $tags->pluck('name', 'id'), null, ['class' => 'form-control multi-tag'.($errors->has('tags') ? ' is-invalid' : ''), 'multiple' => 'multiple']) !!}
                            @if ($errors->has('tags'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('tags') !!}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            {!! Form::label('formations', __('Formations'), ['class' => '']) !!}
                            {!! Form::select('formations[]', $formations->pluck('name', 'id'), null, ['class' => 'form-control tag'.($errors->has('formations') ? ' is-invalid' : ''), 'multiple' => 'multiple']) !!}
                            @if ($errors->has('formations'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('formations') !!}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            {!! Form::submit(__('Valider'), ['class' => 'btn btn-primary float-right']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection