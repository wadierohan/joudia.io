@extends('layouts.'.( auth()->guard('subscriber')->check() ? 'subscriber' : 'app'))

@section('content')
    <div class="container">
        <ul class="list-group">
            <li class="list-group-item active bg-dark border-dark text-center">
                <a class="profil-header" href="{{ url('mailto:'.$user->email) }}" target="blank">
                    <img src="{{asset('img/profil-photo.png')}}" class="img-thumbnail rounded-circle profil-photo" alt="">
                    <div>
                        {{ $user->email }} ({{ $user->name }})
                    </div>
                </a>
            </li>
            @foreach ($user->formations as $row)
            <a class="list-group-item text-center" href="{{ route('formateur.formation', [$user->domain, $row->url]) }}">{{ $row->name }}</a>
            @endforeach
        </ul>
    </div>

@endsection