@extends('layouts.app')

@section('content')
@include('components.page-header', ['breadcrumb' => [__('Formation'), $formation->name, __('Modules'), $module->name], 'new_btn_link' => ''])
<div class="row">
    <div class="col-md-6 offset-md-3">
        <div class="card shadow">
            <div class="card-body">
                {!! Form::open([ 'method'  => 'patch', 'route' => [ 'modules.update', $formation->id, $module->id ], 'autocomplete' => 'off', 'files' => true ]) !!}
                    <div class="form-group">
                        {!! Form::label('name', __('Nom'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::text('name', $module->name, ['class' => 'form-control'.($errors->has('name') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('name') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('desc', __('Description'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::textarea('desc', $module->desc, ['class' => 'form-control'.($errors->has('desc') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('desc'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('desc') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group form-check">
                        {!! Form::checkbox('enabled', true, $module->enabled, ['class' => 'form-check-input'.($errors->has('enabled') ? ' is-invalid' : ''), 'id' => 'enabled']) !!}
                        {!! Form::label('enabled', __('Active'), ['class' => 'form-check-label']) !!}
                        @if ($errors->has('enabled'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('enabled') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group text-right">
                        {!! Form::submit(__('Valider'), ['class' => 'btn btn-primary']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection