@extends('layouts.app')

@section('content')
    @include('components.page-header', ['breadcrumb' => [__('Formation'), $formation->name, __('Modules')], 'new_btn_link' => route('modules.create', $formation->id)])

    <div class="table-responsive shadow">
        <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                    <th>{!! __('Nom') !!}</th>
                    <th>{!! __('Active') !!}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($formation->modules as $row)
                <tr class="trigger-options" data-toggle="tooltip" data-placement="top" data-html="true" title="{!! $row->desc !!}">
                    <td><a href="{!! route('chapters.index', ['formation' => $formation->id, 'module' => $row->id]) !!}">{!! $row->name !!}</a></td>
                    <td>{!! $row->enabled !!}</td>
                    <td>
                        <div class="options-hover float-right">
                            <a href="{!!route('modules.edit', ['formation' => $formation->id, 'module' => $row->id])!!}"><i class="fas fa-edit text-dark"></i></a>
                            <i class="fas fa-trash-alt delete-submit-form text-dark"></i>
                            {!! Form::open([ 'method'  => 'delete', 'route' => [ 'modules.destroy', $formation->id, $row->id ]]) !!}{!! Form::close() !!}
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@endsection