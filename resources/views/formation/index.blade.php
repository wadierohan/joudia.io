@extends('layouts.app')

@section('content')
    @include('components.page-header', ['breadcrumb' => [__('Formations')], 'new_btn_link' => route('formations.create')])

    <div class="table-responsive shadow">
        <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                    <th>{!! __('Nom') !!}</th>
                    <th>{!! __('Lien') !!}</th>
                    <th>{!! __('Active') !!}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach (Auth::user()->formations as $row)
                <tr class="trigger-options" data-toggle="tooltip" data-placement="top" data-html="true" title="{!! $row->desc !!}">
                    <td><a href="{!! route('modules.index', $row->id) !!}">{!! $row->img ? Html::image(asset('storage/'.Auth::id().'/images/'.$row->img), 'img '.$row->name, ['class' => 'img-fluid img-thumbnail formation-thumbnail']) : NULL !!} {!! $row->name !!}</a></td>
                    <td>
                        <div class="input-group">
                            <input id="url-{{ $row->id }}" type="text" class="form-control" aria-describedby="url-{{ $row->id }}-btn" value="{!! route('formateur.formation', [Auth::user()->domain, $row->url]) !!}">
                            <div class="input-group-append">
                                <button data-clipboard-target="#url-{{ $row->id }}" class="btn btn-outline-secondary copy-clipboard" type="button" id="url-{{ $row->id }}-btn">{!! __('Copier') !!}</button>
                            </div>
                        </div>
                    </td>
                    <td>{!! $row->enabled ? 'Oui' : 'Non' !!}</td>
                    <td>
                        <div class="options-hover float-right">
                            <a href="{!!route('formations.edit', $row->id)!!}"><i class="fas fa-edit text-dark"></i></a>
                            <i class="fas fa-trash-alt delete-submit-form text-dark"></i>
                            {!! Form::open([ 'method'  => 'delete', 'route' => [ 'formations.destroy', $row->id ] ]) !!}{!! Form::close() !!}
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection