@extends('layouts.app')

@section('content')
<div class="row">
    <div id="main-title" class="col-md-12">
        <div class="float-left">
            <div id="page-title">{!! __('Formation') !!}</div>
        </div>
        <div class="float-right">
            
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                {!! Form::open([ 'method'  => 'POST', 'route' => [ 'formations.store' ], 'autocomplete' => 'off', 'files' => true ]) !!}
                    <div class="form-group">
                        {!! Form::label('name', __('Nom'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::text('name', null, ['class' => 'form-control'.($errors->has('name') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('name') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('desc', __('Description'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::textarea('desc', null, ['class' => 'form-control'.($errors->has('desc') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('desc'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('desc') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('url', __('Lien'), ['class' => 'col-form-label text-md-right']) !!}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">{{route('formateur.formationraw', Auth::user()->domain)}}/</div>
                            </div>
                            {!! Form::text('url', null, ['class' => 'form-control'.($errors->has('url') ? ' is-invalid' : '')]) !!}
                            @if ($errors->has('url'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('url') !!}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="custom-file">
                            {!! Form::file('img', ['class' => 'custom-file-input'.($errors->has('img') ? ' is-invalid' : '')]) !!}
                            {!! Form::label('img', __('Image'), ['class' => 'custom-file-label']) !!}
                            @if ($errors->has('img'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('img') !!}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group form-check">
                        {!! Form::checkbox('enabled', true, true, ['class' => 'form-check-input'.($errors->has('enabled') ? ' is-invalid' : ''), 'id' => 'enabled']) !!}
                        {!! Form::label('enabled', __('Active'), ['class' => 'form-check-label']) !!}
                        @if ($errors->has('enabled'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('enabled') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::submit(__('Valider'), ['class' => 'btn bg-primary-theme']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection