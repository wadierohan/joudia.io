@extends('layouts.app')

@section('content')
    @include('components.page-header', ['breadcrumb' => [__('Queues')], 'new_btn_link' => ''])

    <div class="table-responsive shadow">
        <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                    <th>{!! __('Plannification/Campagne') !!}</th>
                    <th>{!! __('Newsletter') !!}</th>
                    <th>{!! __('Abonné') !!}</th>
                    <th>{!! __('Essai d\'envoi échoué') !!}</th>
                    <th>{!! __('Date d\'envoi') !!}</th>
                    <th>{!! __('Envoyé') !!}</th>
                    <th>{!! __('Ouvert') !!}</th>
                    <th>{!! __('Date d\'ouverture') !!}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($queues as $queue)
                <tr class="trigger-options">
                    <td>{!! $queue->schedule ? $queue->schedule->name : ($queue->campaignprocess ? $queue->campaignprocess->campaignStep->campaign->name : null) !!}</td>
                    <td>{!! $queue->newsletter->name !!}</td>
                    <td>{!! $queue->subscriber->email !!}</td>
                    <td>{!! $queue->try !!}</td>
                    <td>{!! $queue->send_at !!}</td>
                    <td>{!! $queue->sent ? __('Oui') : __('Non') !!}</td>
                    <td>{!! $queue->opened ? __('Oui') : __('Non') !!}</td>
                    <td>{!! $queue->opened_at !!}</td>
                    <td>
                        <div class="options-hover float-right">
                            <i class="fas fa-trash-alt delete-submit-form text-dark"></i>
                            {!! Form::open([ 'method'  => 'delete', 'route' => [ 'queues.destroy', $queue->id ] ]) !!}{!! Form::close() !!}
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection