@extends('layouts.subscriber')

@section('content')
    <div id="form-container" class="centred">
        <div id="login-form" class="card">
            <div class="card-header bg-dark text-light text-center">
                <h1>@lang('Connexion')</h1>
            </div>
            <div class="card-body">
                <form method="POST" action="{!! route('subscriber.login', $subdomain) !!}">
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label class="sr-only" for="email">@lang('Email')</label>
                            <div class="input-group">
                                <input placeholder="@lang('Email')" aria-describedby="email-addon" id="email" type="email" class="form-control {!! $errors->has('email') ? 'is-invalid' : '' !!}" name="email" value="{!! old('email') !!}" required autofocus>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="email-addon"><i class="fas fa-at"></i></span>
                                </div>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $errors->first('email') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label class="sr-only" for="password">@lang('Mot de passe')</label>
                            <div class="input-group">
                                <input placeholder="@lang('Mot de passe')" aria-describedby="password-addon" id="password" type="password" class="form-control {!! $errors->has('password') ? 'is-invalid' : '' !!}" name="password" required>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="password-addon"><i class="fas fa-lock"></i></span>
                                </div>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $errors->first('password') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {!! old('remember') ? 'checked' : '' !!}>

                                <label class="form-check-label" for="remember">
                                    @lang('Se rappeler de moi')
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">
                                @lang('Connexion')
                            </button>

                            <a class="btn btn-link float-right" href="{!! route('password.request') !!}">
                                @lang('Mot de passe oublié?')
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
