@extends('layouts.subscriber')

@section('content')
    <div id="form-container" class="centred">
        <div class="card">
            <div class="card-header bg-dark text-light text-center">{!! __('Réinitialisation du mot de passe') !!}</div>

            <div class="card-body">
                <form method="POST" action="{!! route('subscriber.password.update', $subdomain) !!}">
                    @csrf

                    <input type="hidden" name="token" value="{!! $token !!}">

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label class="sr-only" for="email">@lang('Email')</label>
                            <div class="input-group">
                                <input placeholder="@lang('Email')" aria-describedby="email-addon" id="email" type="email" class="form-control {!! $errors->has('email') ? 'is-invalid' : '' !!}" name="email" value="{!! old('email') !!}" required autofocus>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="email-addon"><i class="fas fa-at"></i></span>
                                </div>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $errors->first('email') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label class="sr-only" for="password">@lang('Mot de passe')</label>
                            <div class="input-group">
                                <input placeholder="@lang('Mot de passe')" aria-describedby="password-addon" id="password" type="password" class="form-control {!! $errors->has('password') ? 'is-invalid' : '' !!}" name="password" required>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="password-addon"><i class="fas fa-lock"></i></span>
                                </div>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $errors->first('password') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label class="sr-only" for="password_confirmation">@lang('Confirmation du mot de passe')</label>
                            <div class="input-group">
                                <input placeholder="@lang('Confirmation du mot de passe')" aria-describedby="password_confirmation-addon" id="password_confirmation" type="password" class="form-control {!! $errors->has('password_confirmation') ? 'is-invalid' : '' !!}" name="password_confirmation" required>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="password_confirmation-addon"><i class="fas fa-lock"></i></span>
                                </div>
                                @if ($errors->has('password_confirmation'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $errors->first('password_confirmation') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary btn-block">
                                {!! __('Réinitialisation du mot de passe') !!}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
