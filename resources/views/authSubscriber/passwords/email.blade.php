@extends('layouts.subscriber')

@section('content')
    <div id="form-container" class="centred">
        <div class="card">
            <div class="card-header bg-dark text-light text-center">{!! __('Réinitialisation du mot de passe') !!}</div>

            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {!! session('status') !!}
                    </div>
                @endif

                <form method="POST" action="{!! route('subscriber.password.email', $subdomain) !!}">
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label class="sr-only" for="email">@lang('Email')</label>
                            <div class="input-group">
                                <input placeholder="@lang('Email')" aria-describedby="email-addon" id="email" type="email" class="form-control {!! $errors->has('email') ? 'is-invalid' : '' !!}" name="email" value="{!! old('email') !!}" required autofocus>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="email-addon"><i class="fas fa-at"></i></span>
                                </div>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $errors->first('email') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary btn-block">
                                {!! __('Envoyer le lien de réinitialisation') !!}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
