@extends('layouts.app')

@section('content')
    @include('components.page-header', ['breadcrumb' => [__('Plannifications')], 'new_btn_link' => route('schedules.create')])

    <div class="table-responsive shadow">
        <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                    <th>{!! __('Nom') !!}</th>
                    <th>{!! __('Newsletter') !!}</th>
                    <th>{!! __('Tags') !!}</th>
                    <th>{!! __('Date d\'envoi') !!}</th>
                    <th>{!! __('Etat') !!}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach (Auth::user()->schedules as $row)
                <tr class="trigger-options">
                    <td>{!! $row->name !!}</td>
                    <td>{!! $row->newsletter->name !!}</td>
                    <td>
                        {!! $row->tags_type == 'alltags' ? __('Tous les tags') : ($row->tags_type == 'once' ? __('Qui ont un des tags suivants') : __('Qui ont tous les tags suivants')) !!}
                        @if($row->tags_type != 'alltags')
                        @foreach ($row->tags as $tag)
                            <div class="tag">{!! $tag->name !!}</div>
                        @endforeach
                        @endif
                    </td>
                    <td>{!! $row->queued_at !!}</td>
                    <td>{!! $row->queued ? __('Envoyé') : __('En attente') !!}</td>
                    <td>
                        <div class="options-hover float-right">
                            @if(!$row->queued)
                                <a data-toggle="tooltip" data-placement="top" title="{!! __('Modifier') !!}" href="{!!route('schedules.edit', $row->id)!!}"><i class="fas fa-edit text-dark"></i></a>
                            @else
                                <a data-toggle="tooltip" data-placement="top" title="{!! __('Queue') !!}" href="{!!route('queues.schedule', $row->id)!!}"><i class="fas fa-layer-group text-dark"></i></a>
                            @endif
                            <i class="fas fa-trash-alt delete-submit-form text-dark"></i>
                            {!! Form::open([ 'method'  => 'delete', 'route' => [ 'schedules.destroy', $row->id ] ]) !!}{!! Form::close() !!}
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection