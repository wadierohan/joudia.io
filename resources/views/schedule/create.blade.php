@extends('layouts.app')

@section('content')
@include('components.page-header', ['breadcrumb' => [__('Plannification'), __('Nouveau')], 'new_btn_link' => ''])
<div class="row">
    <div class="col-md-6 offset-md-3">
        <div class="card shadow">
            <div class="card-body">
                {!! Form::open([ 'method'  => 'POST', 'route' => [ 'schedules.store' ], 'autocomplete' => 'off' ]) !!}
                    <div class="form-group">
                        {!! Form::label('name', __('Nom'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::text('name', null, ['class' => 'form-control'.($errors->has('name') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('name') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('newsletter_id', __('Newsletter'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::select('newsletter_id', Auth::user()->newsletters->where('type', 'news')->pluck('name', 'id'), null, ['class' => 'form-control'.($errors->has('newsletter_id') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('newsletter_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('newsletter_id') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('tags', __('Tags'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::select('tags_type', ['alltags' => __('Tous les tags'), 'all' => __('Qui ont tous les tags suivants'), 'once' => __('Qui ont au moins un des tags suivants'),], null, ['class' => 'form-control'.($errors->has('tags_type') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('tags_type'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('tags_type') !!}</strong>
                            </span>
                        @endif
                        {!! Form::select('tags[]', Auth::user()->tags->pluck('name', 'id'), null, ['class' => 'multi-tag form-control'.($errors->has('tags') ? ' is-invalid' : ''), 'multiple' => 'multiple', 'disabled' => 'disabled']) !!}
                        @if ($errors->has('tags'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('tags') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('queued_at', __('Date d\'envoi'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::text('queued_at', null, ['class' => 'form-control datetimepicker'.($errors->has('queued_at') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('queued_at'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('queued_at') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group text-right">
                        {!! Form::submit(__('Valider'), ['class' => 'btn btn-primary']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script>
        $(function(){
            var tags_type = $('select[name="tags_type"]').val();
            if(tags_type == 'alltags'){
                $('select[name="tags[]"]').prop('disabled', true);
            }else{
                $('select[name="tags[]"]').prop('disabled', false);
            }

            $('select[name="tags_type"]').change(function(){
                var tags_type = $('select[name="tags_type"]').val();
                if(tags_type == 'alltags'){
                    $('select[name="tags[]"]').prop('disabled', true);
                }else{
                    $('select[name="tags[]"]').prop('disabled', false);
                }
            })
        })
    </script>
@endsection