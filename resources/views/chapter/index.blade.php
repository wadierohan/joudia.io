@extends('layouts.app')

@section('content')
    @include('components.page-header', ['breadcrumb' => [
            __('Formation'), 
            $formation->name, 
            __('Module'),
            $module->name,
            __('Chapitres')
        ], 'new_btn_link' => route('chapters.create', [$formation->id, $module->id])])

    <div class="table-responsive shadow">
        <table class="table table-striped">
            <thead class="thead-dark">
                        <tr>
                            <th>{!! __('Nom') !!}</th>
                            <th>{!! __('Active') !!}</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($module->chapters as $row)
                        <tr class="trigger-options" data-toggle="tooltip" data-placement="top" data-html="true" title="{{ $row->desc }}">
                            <td><a href="{!! route('chapters.show', ['formation' => $formation->id, 'module' => $module->id, 'chapter' => $row->id]) !!}">{!! $row->name !!}</a></td>
                            <td>{!! $row->enabled !!}</td>
                            <td>
                                <div class="options-hover float-right">
                                    <a href="{!!route('chapters.wysiwyg', ['formation' => $formation->id, 'module' => $module->id, 'chapter' => $row->id])!!}"><i class="fas fa-file-alt text-dark"></i></a>
                                    <a href="{!!route('chapters.edit', ['formation' => $formation->id, 'module' => $module->id, 'chapter' => $row->id])!!}"><i class="fas fa-edit text-dark"></i></a>
                                    <i class="fas fa-trash-alt delete-submit-form text-dark"></i>
                                    {!! Form::open([ 'method'  => 'delete', 'route' => [ 'chapters.destroy', $formation->id, $module->id, $row->id ]]) !!}{!! Form::close() !!}
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection