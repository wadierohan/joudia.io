@extends('layouts.'.( auth()->guard('subscriber')->check() ? 'subscriber' : 'app'))

@section('content')
<div id="formation-header">
        <div class="row">
            <div class="col-md-3">
                <div class="user-info">
                    <img src="{{asset('img/profil-photo.png')}}" class="img-thumbnail rounded-circle profil-photo" alt="">
                    {{ $chapter->module->formation->user->name }}
                </div>
            </div>
            <div class="col-md-9">
                <div class="formation-title">
                    <h1>{{$chapter->module->formation->name}}</h1>
                    <h2>{{$chapter->module->name}}</h2>
                    <h3>{{$chapter->name}}</h3>
                </div>
            </div>
        </div>
    </div>
<div class="row">
    <div class="col-md-3">
        <div id="left" class="shadow">
            <div id="formation-title">
                {{$chapter->module->formation->name}}
            </div>

            <ul class="nav menu">
                {{-- MODULES --}}
                @foreach($chapter->module->formation->modules as $rowModule)
                <li class="module parent">
                    <div>
                        <span data-toggle="collapse" data-target="#module{{$rowModule->id}}" aria-expanded="{{$rowModule->id == $chapter->module->id ? 'true' : 'false'}}" aria-controls="module{{$rowModule->id}}" class="sign {{$rowModule->id == $chapter->module->id ? '' : 'collapsed'}}"><i class="fas fa-{{$rowModule->id == $chapter->module->id ? 'minus' : 'plus'}}"></i></span>
                        <a href="{{ auth()->guard('subscriber')->check() ? route('formateur.module', ['subdomain' => $subdomain, 'url' => $url, 'module' => $rowModule->id]) : route('modules.show', ['formation' => $rowModule->formation->id, 'module' => $rowModule->id]) }}"><span class="lbl">{{$rowModule->name}}</span></a>
                    </div>
                    <ul class="children nav-child unstyled small collapse {{$rowModule->id == $chapter->module->id ? 'show' : ''}}" id="module{{$rowModule->id}}">
                        {{-- CHAPTERS --}}
                        @foreach($rowModule->chapters as $rowChapter)
                        <li class="chapter {{$rowChapter->id == $chapter->id ? 'active' : ''}}">
                            <a class="" href="{{ auth()->guard('subscriber')->check() ? route('formateur.chapter', ['subdomain' => $subdomain, 'url' => $url, 'module' => $rowChapter->module->id, 'chapter' => $rowChapter->id]) : route('chapters.show', ['formation' => $rowChapter->module->formation->id, 'module' => $rowChapter->module->id, 'chapter' => $rowChapter->id]) }}">
                                <span class="sign"><i class="fas fa-play"></i></span>
                                <span class="lbl">{{$rowChapter->name}}</span>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="col-md-9">
        <div class="shadow">
            {!! $chapter->html !!}
        </div>
    </div>
</div>
@endsection