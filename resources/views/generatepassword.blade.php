@extends('layouts.email', ['title' => $subject])

@section('content')
    <p>Bienvenue <strong>{{$email}}</strong> sur <a href="{{route('home')}}">{{route('home')}}</a></p>
    <p>Votre identifiant est : <strong>{{$email}}</strong></p>
    <p>Votre mot de passe est : <strong>{{$password}}</strong></p>
    <p>Cordialement,<br>L'équipe <a href="{{route('home')}}">{{route('home')}}</a></p>
@endsection