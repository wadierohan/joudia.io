@extends('layouts.app')

@section('content')
@include('components.page-header', ['breadcrumb' => [__('Campagne'), $campaign->name], 'new_btn_link' => ''])
<div class="row">
    <div id="page-container">
        <div id="page" class="shadow">
            {!! Form::open([ 'method'  => 'patch', 'route' => [ 'campaigns.automate.update', $campaign->id ], 'autocomplete' => 'off' ]) !!}
                <div id="campaign-steps-container">
                    <div id="click-to-add">{!! __('Cliquez pour ajouter une étape') !!}</div>
                    <ul id="campaign-steps-ul" class="list-group">
                    @foreach($campaign->campaignSteps as $campaignstep)
                        <li class="step">
                            <i class="fas fa-sort step-handle step-options"></i>
                            <i class="fas fa-times step-delete step-options"></i>
                            <div class="step-params">
                                <div class="form-row">
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('newsletter_id', __('Envoyer la newsletter'), ['class' => 'col-form-label']) !!}
                                        {!! Form::select('step[newsletter_id][]', Auth::user()->newsletters()->where('type', 'news')->pluck('newsletters.name', 'newsletters.id'), $campaignstep->newsletter_id, ['class' => 'form-control']) !!}
                                    </div>
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('human_delay', __('Après'), ['class' => 'col-form-label']) !!}
                                        {!! Form::hidden('step[delay][]', $campaignstep->delay, ['class' => 'delay']) !!}
                                        {!! Form::text('human_delay', null, ['class' => 'durationpicker form-control', 'placeholder' => __('Immediatement')]) !!}
                                    </div>
                                </div>
                            </div>
                            <i class="fas fa-plus add-step-before step-options"></i>
                            <i class="fas fa-plus add-step-after step-options"></i>
                        </li>
                    @endforeach
                    </ul>
                </div>
                <button type="submit" class="btn btn-primary btn-block">{!! __('Enregistrer') !!}</button>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
@section('script')
    <script>
        $(function(){
            step = '<li class="step">\
                <i class="fas fa-sort step-handle step-options"></i>\
                <i class="fas fa-times step-delete step-options"></i>\
                <div class="step-params">\
                    <div class="form-row">\
                        <div class="form-group col-sm-6">\
                            {!! Form::label('newsletter_id', __('Envoyer la newsletter'), ['class' => 'col-form-label']) !!}\
                            {!! Form::select('step[newsletter_id][]', Auth::user()->newsletters()->where('type', 'news')->pluck('newsletters.name', 'newsletters.id'), null, ['class' => 'form-control']) !!}\
                        </div>\
                        <div class="form-group col-sm-6">\
                            {!! Form::label('human_delay', __('Après'), ['class' => 'col-form-label']) !!}\
                            {!! Form::hidden('step[delay][]', null, ['class' => 'delay']) !!}\
                            {!! Form::text('human_delay', null, ['class' => 'durationpicker form-control', 'placeholder' => __('Immediatement')]) !!}\
                        </div>\
                    </div>\
                </div>\
                <i class="fas fa-plus add-step-before step-options"></i>\
                <i class="fas fa-plus add-step-after step-options"></i>\
            </li>';
            function isEmpty(){
                if( !$.trim( $('#campaign-steps-container > #campaign-steps-ul').html() ).length ) {
                    $('#campaign-steps-container > #click-to-add').show();
                }else{
                    $('#campaign-steps-container > #click-to-add').hide();
                }
            }
            
            function init(){
                $('.durationpicker').timeDurationPicker({
                    lang: 'fr_FR',
                    defaultValue: function(element) {
                        return element.siblings('.delay').val();
                    },
                    onSelect: function(element, seconds, duration) {
                        element.siblings('.delay').val(seconds);
                        element.val(duration);
                    },
                    years: false,
                    months: false,
                    days: true,
                    hours: true,
                    minutes: true,
                    seconds: false,
                });
            }

            isEmpty();
            init();

            $('#click-to-add').click(function(){
                $('#campaign-steps-ul').append(step);
                isEmpty();
                init();
            })

            $('#campaign-steps-container > #campaign-steps-ul').on('click', '.add-step-after', function(){
                $(this).closest('.step').after(step);
                init();
            })

            $('#campaign-steps-container > #campaign-steps-ul').on('click', '.add-step-before', function(){
                $(this).closest('.step').before(step);
                init();
            })

            $('#campaign-steps-container > #campaign-steps-ul').on('click', '.step-delete', function(){
                $(this).closest('.step').remove();
                isEmpty();
            })

            $('#campaign-steps-container > #campaign-steps-ul').sortable({
                revert: true,
                placeholder: "ui-state-highlight",
                handle: ".step-handle",
                axis: "y",
                start: function(e, ui){
                    ui.placeholder.height(ui.item.height());
                },
                helper: "clone",
            });
        })
    </script>
@endsection