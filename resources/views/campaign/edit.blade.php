@extends('layouts.app')

@section('content')
@include('components.page-header', ['breadcrumb' => [__('Campagne'), $campaign->name], 'new_btn_link' => ''])
<div class="row">
    <div class="col-md-6 offset-md-3">
        <div class="card shadow">
            <div class="card-body">
                {!! Form::open([ 'method'  => 'patch', 'route' => [ 'campaigns.update', $campaign->id ], 'autocomplete' => 'off' ]) !!}
                    <div class="form-group row">
                        {!! Form::label('name', __('Nom'), ['class' => 'col-sm-4 col-form-label text-md-right']) !!}
                        <div class="col-sm-6">
                            {!! Form::text('name', $campaign->name, ['class' => 'form-control'.($errors->has('name') ? ' is-invalid' : ''), 'required' => 'required']) !!}
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('name') !!}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('trigger', __('Déclencheur'), ['class' => 'col-sm-4 col-form-label text-md-right']) !!}
                        <div class="col-sm-6">
                            {!! Form::select('trigger', ['at_date' => __('À la date'), 'new_form_sub' => 'Nouveau inscrit au formulaire'], $campaign->trigger, ['class' => 'form-control'.($errors->has('mailserver_id') ? ' is-invalid' : ''), 'required' => 'required']) !!}
                            @if ($errors->has('trigger'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('trigger') !!}</strong>
                                </span>
                            @endif
                            <div class="trigger-params-container new_form_sub">
                                {!! Form::select('trigger_value', Auth::user()->forms()->pluck('forms.name', 'forms.id'), ($campaign->trigger == 'new_form_sub' ? $campaign->trigger_value : null), ['class' => 'trigger-params form-control'.($errors->has('trigger_value') ? ' is-invalid' : ''), 'disabled' => 'disabled']) !!}
                                @if ($errors->has('trigger_value'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('trigger_value') !!}</strong>
                                </span>
                            @endif
                            </div>
                            <div class="trigger-params-container at_date">
                                {!! Form::text('trigger_value', ($campaign->trigger == 'at_date' ? $campaign->trigger_value : null), ['class' => 'trigger-params form-control datetimepicker'.($errors->has('trigger_value') ? ' is-invalid' : ''), 'disabled' => 'disabled']) !!}
                                @if ($errors->has('trigger_value'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('trigger_value') !!}</strong>
                                </span>
                            @endif
                            </div>
                        </div>
                    </div>
                    <div class="trigger-params-container at_date">
                        <div class="form-group row">
                            {!! Form::label('tags', __('Tags'), ['class' => 'col-sm-4 col-form-label text-md-right']) !!}
                            <div class="col-sm-6">
                                {!! Form::select('tags_type', ['alltags' => __('Tous les tags'), 'all' => __('Qui ont tous les tags suivants'), 'once' => __('Qui ont au moins un des tags suivants'),], $campaign->tags_type, ['class' => 'trigger-params form-control'.($errors->has('tags_type') ? ' is-invalid' : '')]) !!}
                                @if ($errors->has('tags_type'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $errors->first('tags_type') !!}</strong>
                                    </span>
                                @endif
                                {!! Form::select('tags[]', Auth::user()->tags->pluck('name', 'id'), $campaign->tags, ['class' => 'trigger-params form-control'.($errors->has('tags') ? ' is-invalid' : ''), 'multiple' => 'multiple', 'disabled' => 'disabled']) !!}
                                @if ($errors->has('tags'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $errors->first('tags') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="offset-sm-4 col-sm-6 text-right">
                            {!! Form::submit(__('Valider'), ['class' => 'btn btn-primary']) !!}
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script>
        $(function(){
            var trigger = $('select[name="trigger"]').val();
            $('.trigger-params-container').hide();
            $('.trigger-params').prop('disabled', true);
            $('.'+trigger).find('.trigger-params').prop('disabled', false);
            $('.'+trigger).show();

            $('select[name="trigger"]').change(function(){
                var trigger = $('select[name="trigger"]').val();
                $('.trigger-params-container').hide();
                $('.trigger-params').prop('disabled', true);
                $('.'+trigger).find('.trigger-params').prop('disabled', false);
                $('.'+trigger).show();

                var tags_type = $('select[name="tags_type"]').val();
                if(tags_type == 'alltags'){
                    $('select[name="tags[]"]').prop('disabled', true);
                }else{
                    $('select[name="tags[]"]').prop('disabled', false);
                }
            })



            var tags_type = $('select[name="tags_type"]').val();
            if(tags_type == 'alltags'){
                $('select[name="tags[]"]').prop('disabled', true);
            }else{
                $('select[name="tags[]"]').prop('disabled', false);
            }

            $('select[name="tags_type"]').change(function(){
                var tags_type = $('select[name="tags_type"]').val();
                if(tags_type == 'alltags'){
                    $('select[name="tags[]"]').prop('disabled', true);
                }else{
                    $('select[name="tags[]"]').prop('disabled', false);
                }
            })
        })
    </script>
@endsection