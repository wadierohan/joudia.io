@extends('layouts.app')

@section('content')
    @include('components.page-header', ['breadcrumb' => [__('Campagnes')], 'new_btn_link' => route('campaigns.create')])

    <div class="table-responsive shadow">
        <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                    <th>{!! __('Nom') !!}</th>
                    <th>{!! __('Déclencheur') !!}</th>
                    <th>{!! __('Valeur') !!}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach (Auth::user()->campaigns as $row)
                <tr class="trigger-options">
                    <td>{!! $row->name !!}</td>
                    <td>
                        @if($row->trigger == 'new_form_sub')    {!! __('Nouveau inscrit au formulaire') !!}
                        @elseif($row->trigger == 'at_date')    {!! __('À la date') !!}
                        @endif
                    </td>
                    <td>
                        @if($row->trigger == 'new_form_sub')    {!! $row->form->name !!}
                        @elseif($row->trigger == 'at_date')    {!! $row->trigger_value !!}
                        @endif
                    </td>
                    <td>
                        <div class="options-hover float-right">
                            <a href="{!!route('campaigns.automate', $row->id)!!}"><i class="fas fa-file-alt text-dark"></i></a>
                            <a href="{!!route('campaigns.edit', $row->id)!!}"><i class="fas fa-edit text-dark"></i></a>
                            <i class="fas fa-trash-alt delete-submit-form text-dark"></i>
                            {!! Form::open([ 'method'  => 'delete', 'route' => [ 'campaigns.destroy', $row->id ] ]) !!}{!! Form::close() !!}
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection