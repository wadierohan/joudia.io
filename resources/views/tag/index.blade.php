@extends('layouts.app')

@section('content')
    @include('components.page-header', ['breadcrumb' => [__('Tags')], 'new_btn_link' => route('tags.create')])

    <div class="container">
        <div class="row">
            <div class="table-responsive shadow">
                <table class="table table-striped">
                    <thead class="thead-dark">
                        <tr>
                            <th>{!! __('Nom') !!}</th>
                            <th>{!! __("Inscrits aujourd'hui") !!}</th>
                            <th>{!! __('Inscrits hier') !!}</th>
                            <th>{!! __('Avec ce tag') !!}</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach (Auth::user()->tags as $row)
                        <tr class="trigger-options">
                            <td>{!! $row->name !!}</td>
                            <td>{!! $row->subscribedToday()->count() !!}</td>
                            <td>{!! $row->subscribedYesterday()->count() !!}</td>
                            <td>{!! $row->subscribers()->count() !!}</td>
                            <td>
                                <div class="options-hover float-right">
                                    <a href="{!!route('tags.edit', $row->id)!!}"><i class="fas fa-edit text-dark"></i></a>
                                    <i class="fas fa-trash-alt delete-submit-form text-dark"></i>
                                    {!! Form::open([ 'method'  => 'delete', 'route' => [ 'tags.destroy', $row->id ] ]) !!}{!! Form::close() !!}
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection